package cn.ahren.blog.dao;

import cn.ahren.blog.BlogApplicationTests;
import cn.ahren.blog.dto.ArticleDto;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ArticleDtoDaoTest extends BlogApplicationTests {

    @Autowired
    private ArticleDtoDao articleDtoDao;

    //查找文章（根据文章id）
    @Test
    public void selectById() {
        ArticleDto articleDto=articleDtoDao.selectById("001");
        Assert.assertNotNull(articleDto);
    }

    //查找所有文章
    @Test
    public void selectAll() {
        List<ArticleDto> articleDtoList=articleDtoDao.selectAll(1);
        Assert.assertNotEquals(0,articleDtoList.size());
    }

    //查找文章（根据标签名）
    @Test
    public void selectByTagName() {
        List<ArticleDto> articleDtoList=articleDtoDao.selectByTagName("Java");
        Assert.assertNotEquals(0,articleDtoList.size());
    }

    //查找文章（根据分类名称）
    @Test
    public void selectByCategoryName() {
        List<ArticleDto> articleDtoList=articleDtoDao.selectByCategoryName("Java");
        Assert.assertNotEquals(0,articleDtoList.size());
    }

    //查询文章-根据标题
    @Test
    public void selectByTitle() {
        List<ArticleDto> articleDtoList=articleDtoDao.selectByTitle("Java");
        Assert.assertNotEquals(0,articleDtoList.size());
    }
}