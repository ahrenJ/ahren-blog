package cn.ahren.blog.dao;

import cn.ahren.blog.BlogApplicationTests;
import cn.ahren.blog.entity.Category;
import cn.ahren.blog.vo.CateToArticleNumVo;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CategoryDaoTest extends BlogApplicationTests {

    @Autowired
    private CategoryDao categoryDao;

    //添加分类
    @Test
    public void insert() {
        Category category=new Category();
        category.setName("Python");
        int result=categoryDao.insert(category);
        Assert.assertNotEquals(0,result);
    }

    //修改分类
    @Test
    public void update() {
        Category category=new Category();
        category.setId(1);
        category.setName("Java编程");
        int result=categoryDao.update(category);
        Assert.assertNotEquals(0,result);
    }

    //删除分类
    @Test
    public void delete() {
        int result=categoryDao.delete(5001);
        Assert.assertNotEquals(0,result);
    }

    //根据id查找分类
    @Test
    public void selectById() {
        Category category=categoryDao.selectById(1);
        Assert.assertNotNull(category);
    }

    //根据名称查找分类
    @Test
    public void selectByName() {
        Category category=categoryDao.selectByName("Java编程");
        Assert.assertNotNull(category);
    }

    //查找所有分类
    @Test
    public void selectAll() {
        List<Category> categories=new ArrayList<>();
        categories=categoryDao.selectAll();
        Assert.assertNotEquals(0,categories.size());
    }

    //查询所有分类及对应的文章数量
    @Test
    public void countByArticleNum() {
        List<CateToArticleNumVo> list=categoryDao.countByArticleNum();
        System.out.println(list.toString());
        Assert.assertNotEquals(0,list.size());
    }
}