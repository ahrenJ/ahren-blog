package cn.ahren.blog.dao;

import cn.ahren.blog.BlogApplicationTests;
import cn.ahren.blog.entity.Comment;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static org.junit.Assert.*;

@Component
public class CommentDaoTest extends BlogApplicationTests {

    @Autowired
    private CommentDao commentDao;

    //添加评论
    @Test
    public void insert() {
        Comment comment=new Comment();
        comment.setArticleId("001");
        comment.setUserName("过客1");
        comment.setUserEmail("test@163.com");
        comment.setContent("这是一个评论233333");
        int result=commentDao.insert(comment);
        Assert.assertNotEquals(0,result);
    }

    //删除评论
    @Test
    public void delete() {
        int result=commentDao.delete(101);
        Assert.assertNotEquals(0,result);
    }

    //查找评论
    @Test
    public void selectById() {
        Comment result=commentDao.selectById(100);
        Assert.assertNotNull(result);
    }

    //根据文章id查找评论
    @Test
    public void selectByArticleId() {
        List<Comment> results=commentDao.selectByArticleId("001");
        Assert.assertNotEquals(0,results.size());
    }

    //查找所有评论
    @Test
    public void selectAll() {
        List<Comment> results=commentDao.selectAll();
        Assert.assertNotEquals(0,results.size());
    }
}