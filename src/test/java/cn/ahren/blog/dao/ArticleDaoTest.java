package cn.ahren.blog.dao;

import cn.ahren.blog.BlogApplicationTests;
import cn.ahren.blog.entity.Article;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@Component
public class ArticleDaoTest extends BlogApplicationTests {

    @Autowired
    private ArticleDao articleDao;

    //添加文章
    @Test
    public void insert() {
        Article article=new Article();
        article.setId("001");
        article.setTitle("SpringBoot教程（一）");
        article.setSummary("简介");
        article.setCategoryId(101);
        article.setContent("内容");
        article.setCommentNum(2);
        article.setLikerNum(10);
        article.setState(0);
        int result=articleDao.insert(article);
        Assert.assertNotEquals(0,result);
    }

    //修改文章
    @Test
    public void update() {
        Article article=new Article();
        article.setId("001");
        article.setTitle("SpringBoot教程（一）");
        article.setSummary("更新简介");
        article.setCategoryId(101);
        article.setContent("更新内容");
        article.setState(0);
        int result=articleDao.update(article);
        Assert.assertNotEquals(0,result);
    }

    //查询文章（根据id）
    @Test
    public void selectById() {
        Article article=articleDao.selectById("001");
        Assert.assertNotNull(article);
    }

    //查询所有文章
    @Test
    public void selectAll() {
        List<Article> articles=articleDao.selectAll();
        Assert.assertNotEquals(0,articles.size());
    }

    //查询文章（根据文章id集合）
    @Test
    public void selectByIdList() {
        List<String> idList= Arrays.asList("001","002","003","004","005");
        List<Article> articles=articleDao.selectByIdList(idList);
        Assert.assertEquals(5,articles.size());
    }

    //查询文章（根据分类）
    @Test
    public void selectByCategoryId() {
        List<Article> articles=articleDao.selectByCategoryId(101);
        Assert.assertNotEquals(0,articles.size());
    }

    //删除文章
    @Test
    public void delete() {
        int result=articleDao.delete("009");
        Assert.assertNotEquals(0,result);
    }
}