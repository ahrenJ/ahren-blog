package cn.ahren.blog.dao;

import cn.ahren.blog.BlogApplicationTests;
import cn.ahren.blog.entity.Reply;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static org.junit.Assert.*;

@Component
public class ReplyDaoTest extends BlogApplicationTests {

    @Autowired
    private ReplyDao replyDao;

    //添加子评论
    @Test
    public void insert() {
        Reply reply=new Reply();
        reply.setCommentId(100);
        reply.setUserName("留言者1");
        reply.setUserEmail("reply_test@163.com");
        reply.setContent("秀儿是你吗?");
        int result=replyDao.insert(reply);
        Assert.assertNotEquals(0,result);
    }

    //添加多条回复
    @Test
    public void insertMany() {
        for (int i=2;i<12;i++){
            Reply reply=new Reply();
            reply.setCommentId(100);
            reply.setUserName("留言者"+i);
            reply.setUserEmail("reply_test@163.com");
            reply.setContent("秀儿是你吗?");
            int result=replyDao.insert(reply);
        }
    }

    //删除子评论
    @Test
    public void delete() {
        int result=replyDao.delete(15);
        Assert.assertNotEquals(0,result);
    }

    //根据评论id查找所有子评论
    @Test
    public void selectByCommentId() {
        List<Reply> results=replyDao.selectByCommentId(100);
        Assert.assertNotEquals(0,results.size());
    }

    //查找所有子评论
    @Test
    public void selectAll() {
        List<Reply> results=replyDao.selectAll();
        Assert.assertNotEquals(0,results.size());
    }

    //删除某条评论的所有回复
    @Test
    public void deleteByCommentId() {

    }
}