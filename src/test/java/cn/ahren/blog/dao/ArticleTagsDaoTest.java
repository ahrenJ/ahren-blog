package cn.ahren.blog.dao;

import cn.ahren.blog.BlogApplicationTests;
import cn.ahren.blog.entity.ArticleTags;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.core.AutoConfigureCache;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@Component
public class ArticleTagsDaoTest extends BlogApplicationTests {

    @Autowired
    private ArticleTagsDao articleTagsDao;

    //添加文章-标签关联
    @Test
    public void insert() {
        ArticleTags articleTags=new ArticleTags();
        articleTags.setArticleId("001");
        articleTags.setTagId(501);
        int result=articleTagsDao.insert(articleTags);
        Assert.assertEquals(1,result);
    }

    //批量添加文章-标签关联
    @Test
    public void multipleInsert() {
        List<Integer> tagIds=Arrays.asList(500,501,502);
        int result=articleTagsDao.multipleInsert("008",tagIds);
        Assert.assertNotEquals(0,result);
    }

    //删除文章-标签关联
    @Test
    public void delete() {
        int result=articleTagsDao.delete(2);
        Assert.assertEquals(1,result);
    }

    //删除文章-标签关联（根据文章id）
    @Test
    public void deleteByArticleId() {
        int result=articleTagsDao.deleteByArticleId("002");
        Assert.assertEquals(1,result);
    }

    //查找文章-标签关联（根据标签id）
    @Test
    public void selectByTagId() {
        List<ArticleTags> articleTags=articleTagsDao.selectByTagId(501);
        Assert.assertNotEquals(0,articleTags.size());
    }

    //查询文章-标签关联（根据文章id）
    @Test
    public void selectByArticleId() {
        List<ArticleTags> articleTags=articleTagsDao.selectByArticleId("001");
        Assert.assertNotEquals(0,articleTags.size());
    }

}