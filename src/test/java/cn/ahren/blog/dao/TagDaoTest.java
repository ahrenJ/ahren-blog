package cn.ahren.blog.dao;

import cn.ahren.blog.BlogApplicationTests;
import cn.ahren.blog.entity.Tag;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class TagDaoTest extends BlogApplicationTests {

    @Autowired
    private TagDao tagDao;

    //添加标签
    @Test
    public void insert() {
        Tag tag=new Tag();
        tag.setName("MySQL");
        int result=tagDao.insert(tag);
        Assert.assertNotEquals(0,result);
    }

    //修改标签
    @Test
    public void update() {
        Tag tag=new Tag();
        tag.setId(1);
        tag.setName("学习2");
        int result=tagDao.update(tag);
        Assert.assertNotEquals(0,result);
    }

    //删除标签
    @Test
    public void delete() {
        int result=tagDao.delete(2);
        Assert.assertNotEquals(0,result);
    }

    //查询标签（根据id）
    @Test
    public void selectById() {
        Tag tag =tagDao.selectById(1);
        Assert.assertNotNull(tag);
    }

    //查询标签（根据名称）
    @Test
    public void selectByName() {
        Tag tag=tagDao.selectByName("Java");
        Assert.assertNotNull(tag);
    }

    //查询标签(根据标签名称集合)
    @Test
    public void selectByNameList() {
        List<String> tagNames=Arrays.asList("MySQL","Java","Spring");
        List<Tag> tags=tagDao.selectByNameList(tagNames);
        Assert.assertNotEquals(0,tags.size());
    }

    //查询所有标签
    @Test
    public void selectAll() {
        List<Tag> tags=new ArrayList<>();
        tags=tagDao.selectAll();
        Assert.assertNotEquals(0,tags.size());
    }

    //查询标签（根据标签id集合）
    @Test
    public void selectByIdList() {
        List<Tag> tags=tagDao.selectByIdList(Arrays.asList(500,501,502));
        Assert.assertEquals(3,tags.size());
    }

}