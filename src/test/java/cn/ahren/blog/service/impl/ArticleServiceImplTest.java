package cn.ahren.blog.service.impl;

import cn.ahren.blog.BlogApplicationTests;
import cn.ahren.blog.dto.ArticleDto;
import cn.ahren.blog.entity.Article;
import cn.ahren.blog.entity.Tag;
import cn.ahren.blog.enums.ArticleStateEnum;
import cn.ahren.blog.service.ArticleService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class ArticleServiceImplTest extends BlogApplicationTests {

    private static final String ARTICLE_ID="1553529531715601";

    @Autowired
    private ArticleService articleService;

    //添加文章
    @Test
    public void add() throws Exception{
        ArticleDto articleDto=new ArticleDto();
        articleDto.setTitle("Java集合框架详解");
        articleDto.setSummary("简介");
        articleDto.setContent("内容...");
        articleDto.setCategoryName("Java");
        articleDto.setState(ArticleStateEnum.FORMAL.getCode());
        List<Tag> tags= Arrays.asList(new Tag(501,"Java"),new Tag(502,"Spring框架"));
        articleDto.setTags(tags);
        int result=articleService.add(articleDto);
        Assert.assertNotEquals(0,result);
    }

    //更新文章
    @Test
    public void update() throws Exception{
        ArticleDto articleDto=new ArticleDto();
        articleDto.setId(ARTICLE_ID);
        articleDto.setTitle("MySQL入门（一）");
        articleDto.setSummary("MySQL入门讲解");
        articleDto.setContent("内容...");
        articleDto.setCategoryName("Java");
        articleDto.setState(ArticleStateEnum.DRAFT.getCode());
        List<Tag> tags= Arrays.asList(new Tag(501,"Java"),new Tag(502,"Spring框架"));
        articleDto.setTags(tags);
        int result=articleService.update(articleDto);
        Assert.assertNotEquals(0,result);
    }

    //查询文章（根据文章id）
    @Test
    public void findById() throws Exception{
        ArticleDto articleDto=articleService.findById(ARTICLE_ID);
        Assert.assertNotNull(articleDto);
    }

    //查询文章集合（根据分类）
    @Test
    public void findByCategoryName() throws Exception{
        PageInfo<ArticleDto> pageInfo=articleService.findByCategoryName("Java",1,10);
        Assert.assertNotEquals(0,pageInfo.getSize());
    }

    //查询文章集合（根据标签）
    @Test
    public void findByTagName() {
        PageInfo<ArticleDto> pageInfo=articleService.findByTagName("Java",1,3);
        Assert.assertNotEquals(0,pageInfo.getSize());
    }

    //查询所有文章
    @Test
    public void findAll() {
        PageInfo<ArticleDto> pageInfo=articleService.findAll(1,4,1);
        Assert.assertNotEquals(0,pageInfo.getTotal());
    }

    //删除文章（根据文章id）
    @Test
    public void deleteById() throws Exception{
        int result=articleService.deleteById("003");
        Assert.assertEquals(1,result);
    }

    //统计文章总数-根据条件
    @Test
    public void countByCondition(){
        String value1="101";
        String value2="Spring";
        String value3="Java";
        int countByCategoryId=articleService.countBy(ArticleService.ARTICLE_CATEGORY_ID,value1);
        int countByTitle=articleService.countBy(ArticleService.ARTICLE_TITLE,value2);
        int countByTagName=articleService.countBy(ArticleService.ARTICLE_TAGS_NAME,value3);
        System.out.println(countByCategoryId+","+countByTitle+","+countByTagName);
        Assert.assertNotEquals(3,countByCategoryId);
        Assert.assertNotEquals(2,countByTitle);
        Assert.assertNotEquals(1,countByTagName);
    }
}