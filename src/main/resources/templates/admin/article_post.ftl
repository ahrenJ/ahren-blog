<#-- 文章添加页 -->
<!DOCTYPE html>
<html>
<head>
    <#include "common/head.ftl">
    <title>${BLOG_NAME}-后台</title>
    <meta name="referrer" content="no-referrer"/>
    <link rel="stylesheet" href="/static/AdminLTE-2.4.10/bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="/static/AdminLTE-2.4.10/bower_components/editor.md-master/css/editormd.min.css"/>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #c5c5c5
        }
    </style>
</head>
<body class="layout-boxed skin-black-light sidebar-mini">
<div class="wrapper">
    <!-- Page Header -->
    <#include "common/header.ftl">
    <!-- Left Sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- User Info Panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/static/AdminLTE-2.4.10/dist/img/owner.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>${OWNER_NAME}</p>
                    <a href="javascript:void(0)"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- Left Sidebar-Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">HEADER</li>
                <!-- Home page -->
                <li><a href="/admin/"><i class="fa fa-home"></i> <span>主页</span></a></li>
                <!-- Article Menu -->
                <li class="treeview active">
                    <a href="#"><i class="fa fa-book"></i> <span>文章</span>
                        <span class="pull-right-container">
				        <i class="fa fa-angle-left pull-right"></i>
				      </span>
                    </a>
                    <ul class="treeview-menu menu-open" style="display: block">
                        <li><a href="/admin/article/list"><i class="fa fa-circle-o"></i>文章列表</a></li>
                        <li class="active"><a href="/admin/article/post"><i class="fa fa-circle-o"></i>添加文章</a></li>
                        <li><a href="/admin/category/list"><i class="fa fa-circle-o"></i>分类</a></li>
                        <li><a href="/admin/tag/list"><i class="fa fa-circle-o"></i>标签</a></li>
                    </ul>
                </li>
                <!-- Comment Menu -->
                <li class="treeview">
                    <a href="#"><i class="fa fa-comments-o"></i> <span>评论</span>
                        <span class="pull-right-container">
				        <i class="fa fa-angle-left pull-right"></i>
				      </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/comment/list"><i class="fa fa-circle-o"></i>列表</a></li>
                    </ul>
                </li>
                <li><#-- 系统设置 -->
                    <a href="/admin/config"><i class="fa fa-cog"></i> <span>系统设置</span></a>
                </li>
                <li><a href="/"><i class="fa fa-hand-o-right"></i> <span>返回博客</span></a></li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
    <!-- Page Content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>&nbsp;</h1>
            <ol class="breadcrumb">
                <li><a href="/admin/article/list">文章</a></li>
                <li class="active">添加</li>
            </ol>
        </section>
        <!-- Main Content -->
        <section class="content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid box-default">
                        <div class="box-header">
                            <h3 class="box-title">添加文章</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <!-- 数据表单 -->
                            <form action="/admin/article/add" method="post" role="form">
                                <!-- title input -->
                                <div class="form-group">
                                    <label class="float-none">标题</label>
                                    <input name="title" type="text" class="form-control"
                                           placeholder="请输入标题 ..." style="width: 45%">
                                </div>
                                <!-- summary textarea -->
                                <div class="form-group">
                                    <label>简介</label>
                                    <textarea name="summary" class="form-control" rows="2" placeholder="请输入文章简介 ..."
                                              style="width: 50%"></textarea>
                                </div>

                                <!-- tag select -->
                                <div class="form-group">
                                    <label><i class="fa fa-fw fa-tags"></i>&nbsp;标签</label>
                                    <select name="tagIds" id="tag-select"
                                            class="form-control select2 select2-hidden-accessible" multiple
                                            data-placeholder="选择标签" style="width: 100%" tabindex="-1"
                                            aria-hidden="true">
                                <#list tags as tag>
                                    <option value="${tag.id}">${tag.name}</option>
                                </#list>
                                    </select>
                                </div>

                                <!-- category select -->
                                <div class="form-group">
                                    <label class="float-none">分类</label>
                                    <select name="categoryName" class="form-control" style="width: 20%">
                                <#list categories as category>
                                    <option value="${category.name}">${category.name}</option>
                                </#list>
                                    </select>
                                </div>

                                <!-- editor.md -->
                                <div class="fomr-group">
                                    <label>内容(Markdown)</label>
                                    <div id="test-editormd">
                                        <!-- Markdown text -->
                                        <textarea name="content" style="display: none"></textarea>
                                        <!-- HTML text -->
                                        <textarea class="editormd-html-textarea"></textarea>
                                    </div>
                                </div>

                                <!--save as draft checkbox -->
                                <div class="checkbox">
                                    <label>
                                        <input name="state" value="1" type="checkbox"> 保存为草稿
                                    </label>
                                </div>

                                <!-- button -->
                                <div class="box-footer">
                                    <input type="submit" id="save" value="添加" class="btn btn-flat bg-aqua color-palette"
                                           style="width:80px">
                                    <input type="button" id="back" value="返回"
                                           class="btn btn-flat bg-gray color-palette pull-right" style="width:80px">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<#include "common/script.ftl">
<script src="/static/AdminLTE-2.4.10/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="/static/AdminLTE-2.4.10/bower_components/editor.md-master/editormd.min.js"></script>
<script>
    $(function () {
        //editor.md加载
        var testEditor = editormd("test-editormd", {
            width: "100%",
            height: 530,
            syncScrolling: "single",
            path: "/static/AdminLTE-2.4.10/bower_components/editor.md-master/lib/",
            //这个配置在simple.html中并没有，但是为了能够提交表单，使用这个配置可以让构造出来的HTML代码直接在第二个隐藏的textarea域中，方便post提交表单。
            saveHTMLToTextarea: true,
            toolbarAutoFixed: false
        });
        //初始化标签选择器
        $(".select2").select2({
            closeOnSelect:false
        });
        //返回按钮事件
        $("#back").bind("click", function () {
            window.location.href = "/admin/article/list";
        });
    });
</script>
</body>
</html>