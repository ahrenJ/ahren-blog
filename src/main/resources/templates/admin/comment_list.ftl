<#-- 文章列表页 -->
<!DOCTYPE html>
<html>
<head>
    <#include "common/head.ftl">
    <title>${BLOG_NAME}-后台</title>
</head>
<body class="layout-boxed skin-black-light sidebar-mini">
<div class="wrapper">
    <!-- Page Header -->
    <#include "common/header.ftl">
    <!-- Left Sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- User Info Panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/static/AdminLTE-2.4.10/dist/img/owner.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>${OWNER_NAME}</p>
                    <a href="javascript:void(0)"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- Left Sidebar-Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">HEADER</li>
                <!-- Home page -->
                <li><a href="/admin/"><i class="fa fa-home"></i> <span>主页</span></a></li>
                <!-- Article Menu -->
                <li class="treeview">
                    <a href="#"><i class="fa fa-book"></i> <span>文章</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/article/list"><i class="fa fa-circle-o"></i>文章列表</a></li>
                        <li><a href="/admin/article/post"><i class="fa fa-circle-o"></i>添加文章</a></li>
                        <li><a href="/admin/category/list"><i class="fa fa-circle-o"></i>分类</a></li>
                        <li><a href="/admin/tag/list"><i class="fa fa-circle-o"></i>标签</a></li>
                    </ul>
                </li>
                <!-- Comment Menu -->
                <li class="treeview active">
                    <a href="#"><i class="fa fa-comments-o"></i> <span>评论</span>
                        <span class="pull-right-container">
				        <i class="fa fa-angle-left pull-right"></i>
				      </span>
                    </a>
                    <ul class="treeview-menu menu-open" style="display: block;">
                        <li class="active"><a href="/admin/comment/list"><i class="fa fa-circle-o"></i>列表</a></li>
                    </ul>
                </li>
                <li><#-- 系统设置 -->
                    <a href="/admin/config"><i class="fa fa-cog"></i> <span>系统设置</span></a>
                </li>
                <li><a href="/"><i class="fa fa-hand-o-right"></i> <span>返回博客</span></a></li>
                <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>
    <!-- Page Content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>&nbsp;</h1>
            <ol class="breadcrumb">
                <li><a href="#">评论</a></li>
                <li class="active">列表</li>
            </ol>
        </section>
        <!-- Main Content -->
        <section class="content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header">
                            <h3 class="box-title">评论列表</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th width="40px">ID</th>
                                        <th width="130px">文章ID</th>
                                        <th>用户名</th>
                                        <th width="150px">评论内容</th>
                                        <th width="140px"><i class="fa fa-envelope-o"></i>&nbsp;邮箱</th>
                                        <th width="80px"><i class="fa fa-comment-o"></i>&nbsp;回复数</th>
                                        <th width="135px">评论时间</th>
                                        <th width="80px">操作</th>
                                    </tr>
                                    <#list commentPageInfo.list as comment>
                                    <tr style="padding-top: 3px">
                                        <td>${comment.id}</td>
                                        <td>${comment.articleId}</a></td>
                                        <td>${comment.userName}</td>
                                        <td>${comment.getContent()}</td>
                                        <td>${comment.userEmail}</td>
                                        <td>${comment.replyNum}</td>
                                        <td>${comment.createTime?string("yyyy-MM-dd HH:mm")}</td>
                                        <td>
                                            <a href="/admin/comment/${comment.id}">详情</a>
                                            <a href="/admin/comment/${comment.id}/del?articleId=${comment.getArticleId()}">删除</a>
                                        </td>
                                    </tr>
                                    </#list>
                                </tbody>
                            </table>
                            <div class="col-sm-6">
                                <div class="dataTables_paginate paging_simple_numbers">
                                    <ul class="pagination">
                                        <#if currentPage lte 1>
                                            <li class="paginate_button previous disabled"><a href="#">上一页</a></li>
                                        <#else>
                                            <li><a href="/admin/comment/list?page=${currentPage - 1}&size=${size}">上一页</a></li>
                                        </#if>

                                        <#list 1..commentPageInfo.getPages() as index>
                                            <#if currentPage == index>
                                                <li class="paginate_button active"><a href="#">${index}</a></li>
                                            <#else>
                                                <li class="paginate_button"><a href="/admin/comment/list?page=${index}&size=${size}">${index}</a></li>
                                            </#if>
                                        </#list>

                                        <#if currentPage gte commentPageInfo.getPages()>
                                            <li class="paginate_button next disabled"><a href="#">下一页</a></li>
                                        <#else>
                                            <li class="paginate_button"><a href="/admin/comment/list?page=${currentPage + 1}&size=${size}">下一页</a>
                                            </li>
                                        </#if>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
        </section>
    </div>
</div>
<#include "common/script.ftl">
</body>
</html>