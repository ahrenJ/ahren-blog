<!DOCTYPE html>
<html>
<head>
    <#include "common/head.ftl">
    <title>${BLOG_NAME}</title>
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="javascript:void(0)"><b>${OWNER_NAME}</b>博客后台系统</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">请输入管理员账号与密码</p>

        <form action="/admin/logging" method="post">
            <div class="form-group has-feedback">
                <input name="accountName" type="email" class="form-control" placeholder="Email">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input name="password" type="password" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <button id="logging" type="button" class="btn btn-primary btn-block btn-flat">登录</button>
                </div>
            </div>
        </form>

    </div>
</div>
<#include "common/script.ftl">
<script src="/static/js/jQuery.md5.js"></script>
<script>
    $(function () {
        $("#logging").on("click",function () {
            var accountName=$("input[name='accountName']").val();
            var password=$("input[name='password']").val();
            if (accountName==""||accountName==null){
                alert("账户名不能为空");
                return false;
            }
            if (password==""||password==null){
                alert("密码不能为空");
                return false;
            }
            $("input[name='password']").val($.md5(password));
            $.ajax({
                url:"/admin/logining",
                type:"POST",
                data: JSON.stringify({
                    "accountName":$("input[name='accountName']").val(),
                    "password":$("input[name='password']").val()
                }),
                contentType:"application/json;charset=UTF-8",
                dataType:"text",
                success:function (data) {
                    if (data=="success"){
                        window.location.href="/admin/";
                    } else{
                        alert("账号或密码错误");
                    }
                }
            });
        });
    })
</script>
</body>
</html>
