<#-- 网站系统设置 -->
<!DOCTYPE html>
<html>
<head>
    <#include "common/head.ftl">
    <title>${BLOG_NAME}-后台</title>
</head>
<body class="layout-boxed skin-black-light sidebar-mini">
<div class="wrapper">
    <!-- Page Header -->
    <#include "common/header.ftl">
    <!-- Left Sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- User Info Panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/static/AdminLTE-2.4.10/dist/img/owner.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>${OWNER_NAME}</p>
                    <a href="javascript:void(0)"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- Left Sidebar-Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">HEADER</li>
                <!-- Home page -->
                <li><a href="/admin/"><i class="fa fa-home"></i> <span>主页</span></a></li>
                <!-- Article Menu -->
                <li class="treeview">
                    <a href="#"><i class="fa fa-book"></i> <span>文章</span>
                        <span class="pull-right-container">
				        <i class="fa fa-angle-left pull-right"></i>
				      </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/article/list"><i class="fa fa-circle-o"></i>文章列表</a></li>
                        <li><a href="/admin/article/post"><i class="fa fa-circle-o"></i>添加文章</a></li>
                        <li><a href="/admin/category/list"><i class="fa fa-circle-o"></i>分类</a></li>
                        <li><a href="/admin/tag/list"><i class="fa fa-circle-o"></i>标签</a></li>
                    </ul>
                </li>
                <!-- Comment Menu -->
                <li class="treeview">
                    <a href="#"><i class="fa fa-comments-o"></i> <span>评论</span>
                        <span class="pull-right-container">
				        <i class="fa fa-angle-left pull-right"></i>
				      </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/comment/list"><i class="fa fa-circle-o"></i>列表</a></li>
                    </ul>
                </li>
                <!-- System Setting Menu -->
                <li class="active">
                    <a href="/admin/config"><i class="fa fa-cog"></i> <span>系统设置</span></a>
                </li>
                <li><a href="/"><i class="fa fa-hand-o-right"></i> <span>返回博客</span></a></li>
            </ul>
        </section>
    </aside>
    <!-- Page Content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>系统设置</h1>
            <ol class="breadcrumb">
                <li><a href="/admin">主页</a></li>
                <li class="active">Here</li>
            </ol>
        </section>
        <!-- Main Content -->
        <section class="content container-fluid">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#web-config" data-toggle="tab" aria-expanded="false">网站设置</a></li>
                    <li class=""><a href="#account-config" data-toggle="tab" aria-expanded="false">管理员账户修改</a></li>
                </ul>
                <div class="tab-content">
                    <#-- 网站设置 -->
                    <div class="tab-pane active" id="web-config">
                        <form id="form-config" action="/admin/alter/blogConfig" method="post" class="form-horizontal">
                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">博客名称</label>
                                <div class="col-sm-4">
                                    <input name="blogName" type="text" value="${blogName}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="col-sm-2 control-label">博主署名</label>
                                <div class="col-sm-4">
                                    <input name="ownerName" type="text" value="${ownerName}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="col-sm-2 control-label">博主邮箱</label>
                                <div class="col-sm-4">
                                    <input name="ownerEmail" type="email" value="${ownerEmail}" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">Github链接</label>
                                <div class="col-sm-4">
                                    <input name="githubLink" type="text" value="${githubLink}" class="form-control" placeholder="https://github.com/{UserName}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">博客前台浏览权限</label>
                                <div class="col-md-2">
                                    <select name="openOutside" class="form-control">
                                        <#if openOutside==1>
                                            <option value="1" selected>开放</option>
                                            <option value="0">关闭</option>
                                        <#else>
                                            <option value="1">开放</option>
                                            <option value="0" selected>关闭</option>
                                        </#if>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button id="btn-alterConfig" type="button" class="btn btn-flat btn-danger">确认</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <#-- /.网站设置 -->

                    <#-- 管理员账户修改 -->
                        <div class="tab-pane" id="account-config">
                        <#-- 修改账户表单 -->
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">账户名</label>
                                    <div class="col-sm-4">
                                        <input name="accountName" type="text" value="${accountName}" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">新密码</label>
                                    <div class="col-sm-4">
                                        <input name="password" type="password" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">确认密码</label>
                                    <div class="col-sm-4">
                                        <input name="confirmPwd" type="password" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-4">
                                        <button id="btn-alterAccount" type="button" class="btn btn-flat btn-danger">确认</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    <#-- /.管理员账户修改 -->
                </div>
            </div>
        </section>
    </div>
</div>
<#include "common/script.ftl">
<script src="/static/js/jQuery.md5.js"></script>
<script>
    $(function () {
        $("#btn-alterAccount").on("click", function () {
            var accountName = $("input[name='accountName']").val();
            var password = $("input[name='password']").val();
            var confirmPwd = $("input[name='confirmPwd']").val();
            if (accountName==null||accountName==""){
                alert("账户名不能为空");
                return false;
            }
            if (password==null||password==""){
                alert("密码不能为空");
                return false;
            }
            if (confirmPwd==null||confirmPwd==""||password!=confirmPwd){
                alert("两次密码不一致");
                return false;
            }
            $.ajax({
                url:"/admin/alter/account",
                type:"POST",
                data: JSON.stringify({
                    "accountName":accountName,
                    "password":$.md5(password),
                    "confirmPassword":$.md5(confirmPwd)
                }),
                contentType:"application/json;charset=UTF-8",
                dataType:"text",
                success:function (data) {
                    if (data=="success"){
                        alert("账户修改成功!");
                        window.location.href="/admin/config";
                    } else{
                        alert("修改失败，请检查账户名是否合法或两次密码是否一致");
                    }
                }
            });
        });

        $("#btn-alterConfig").on("click",function () {
            var blogName=$("input[name='blogName']").val();
            var ownerName=$("input[name='ownerName']").val();
            var ownerEmail=$("input[name='ownerEmail']").val();
            var githubLink=$("input[name='githubLink']").val();
            if (blogName==null||blogName==""){
                alert("博客名称不能为空");
                return false
            }
            if (ownerName==null||ownerName==""){
                alert("博主署名不能为空");
                return false
            }
            $("#form-config").submit();
            alert("网站设置修改成功");
        });
    })
</script>
</body>
</html>