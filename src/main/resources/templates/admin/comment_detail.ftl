<#-- 评论详情页 -->
<!DOCTYPE html>
<html>
<head>
    <#include "common/head.ftl">
    <title>${BLOG_NAME}-后台</title>
    <link rel="stylesheet" href="/static/AdminLTE-2.4.10/bower_components/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="/static/AdminLTE-2.4.10/bower_components/editor.md-master/css/editormd.min.css"/>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #c5c5c5
        }
    </style>
</head>
<body class="layout-boxed skin-black-light sidebar-mini">
<div class="wrapper">
    <!-- Page Header -->
    <#include "common/header.ftl">
    <!-- Left Sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- User Info Panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/static/AdminLTE-2.4.10/dist/img/owner.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>${OWNER_NAME}</p>
                    <a href="javascript:void(0)"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- Left Sidebar-Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">HEADER</li>
                <li><a href="/admin/"><i class="fa fa-home"></i> <span>主页</span></a></li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-book"></i> <span>文章</span>
                        <span class="pull-right-container">
				        <i class="fa fa-angle-left pull-right"></i>
				        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/article/list"><i class="fa fa-circle-o"></i>文章列表</a></li>
                        <li><a href="/admin/article/post"><i class="fa fa-circle-o"></i>添加文章</a></li>
                        <li><a href="/admin/category/list"><i class="fa fa-circle-o"></i>分类</a></li>
                        <li><a href="/admin/tag/list"><i class="fa fa-circle-o"></i>标签</a></li>
                    </ul>
                </li>
                <li class="treeview active" style="display: block">
                    <a href="#"><i class="fa fa-comments-o"></i> <span>评论</span>
                        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu menu-open">
                        <li class="active"><a href="/admin/comment/list"><i class="fa fa-circle-o"></i>列表</a></li>
                    </ul>
                </li>
                <li><#-- 系统设置 -->
                    <a href="/admin/config"><i class="fa fa-cog"></i> <span>系统设置</span></a>
                </li>
                <li><a href="/"><i class="fa fa-hand-o-right"></i> <span>返回博客</span></a></li>
            </ul>
        </section>
    </aside>
    <!-- Page Content -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>&nbsp;</h1>
            <ol class="breadcrumb">
                <li><a href="/admin/article/list">文章列表</a></li>
                <li class="active">编辑</li>
            </ol>
        </section>
        <!-- Main Content -->
        <section class="content container-fluid">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">评论详情</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <!-- 数据表单 -->
                    <form class="form-horizontal">
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-2">评论ID</label>${comment.id}
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2">文章ID</label>${comment.articleId}
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2"><i class="fa fa-user"></i>&nbsp;&nbsp;用户名</label>${comment.userName}
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2"><i class="fa fa-envelope-o"></i>&nbsp;&nbsp;邮箱</label>${comment.userEmail}
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2"><i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;评论内容</label>${comment.content}
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2"><i class="fa fa-comment-o"></i>&nbsp;&nbsp;回复数</label>${comment.replyNum}
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2"><i class="fa fa-calendar-minus-o"></i>&nbsp;&nbsp;评论日期</label>${comment.createTime?string("yyyy-MM-dd hh:mm")}
                            </div>
                            <div class="box-footer">
                                <input id="back" type="button" value="返回" class="btn btn-flat btn-default color-palette">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">回复</h3>
                </div>
                <#-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                            <th>评论ID</th>
                            <th><i class="fa fa-user"></i>&nbsp;用户名称</th>
                            <th><i class="fa fa-envelope-o"></i>&nbsp;&nbsp;邮箱</th>
                            <th>被回复者ID</th>
                            <th><i class="fa fa-pencil-square-o"></i>&nbsp;回复内容</th>
                            <th><i class="fa fa-calendar-minus-o"></i>&nbsp;回复时间</th>
                            <th>操作</th>
                        </tbody>
                        <#list replies as reply>
                            <tr>
                                <td>${reply.id}</td>
                                <td>${reply.userName}</td>
                                <td>${reply.userEmail}</td>
                                <#if reply.toId == -1>
                                    <td></td>
                                <#else>
                                    <td>${reply.toId}</td>
                                </#if>
                                <td>${reply.content}</td>
                                <td>${reply.createTime?string("yyyy-MM-dd hh:mm")}</td>
                                <td><a href="/admin/reply/${reply.id}/del?commentId=${comment.id}">删除</a></td>
                            </tr>
                        </#list>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>

<#include "common/script.ftl">
<script>
    $(function () {
        //返回按钮事件
        $("#back").bind("click", function () {
            window.location.href = "/admin/comment/list";
        });
    });
</script>
</body>
</html>