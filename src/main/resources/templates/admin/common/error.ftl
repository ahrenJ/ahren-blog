<#-- 错误页面 -->
<!DOCTYPE html>
<html>
<head>
    <#include "head.ftl">
    <title>${BLOG_NAME}-后台</title>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>
<body class="layout-boxed skin-black-light sidebar-mini">
<div class="wrapper">
    <!-- Page Header -->
    <#include "header.ftl">
    <!-- Left Sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- User Info Panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/static/AdminLTE-2.4.10/dist/img/owner.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>${OWNER_NAME}</p>
                    <a href="javascript:void(0)"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- Left Sidebar-Menu -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">HEADER</li>
                <!-- Home page -->
                <li><a href="/admin/"><i class="fa fa-home"></i> <span>主页</span></a></li>
                <!-- Article Menu -->
                <li class="treeview">
                    <a href="#"><i class="fa fa-book"></i> <span>文章</span>
                        <span class="pull-right-container">
				        <i class="fa fa-angle-left pull-right"></i>
				      </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/article/list"><i class="fa fa-circle-o"></i>列表</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i>添加</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i>分类与标签</a></li>
                    </ul>
                </li>
                <!-- Comment Menu -->
                <li class="treeview">
                    <a href="#"><i class="fa fa-comments-o"></i> <span>评论</span>
                        <span class="pull-right-container">
				        <i class="fa fa-angle-left pull-right"></i>
				      </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i>列表</a></li>
                    </ul>
                </li>
                <!-- System Setting Menu -->
                <li class="treeview">
                    <a href="#"><i class="fa fa-cog"></i> <span>系统</span>
                        <span class="pull-right-container">
				        <i class="fa fa-angle-left pull-right"></i>
				      </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i>网站备份</a></li>
                        <li><a href="#"><i class="fa fa-circle-o"></i>设置</a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="fa fa-hand-o-right"></i> <span>返回博客</span></a></li>
                <!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
    </aside>
    <!-- Page Content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>&nbsp;</h1>
            <ol class="breadcrumb">
                <li><a href="/admin">主页</a></li>
                <li class="active">Error</li>
            </ol>
        </section>
        <!-- Main Content -->
        <section class="content container-fluid">
            <div class="box box-solid box-danger">
                <div class="box-header">
                    <h3 class="box-title">错误!</h3>
                </div>
                <div class="box-body">
                    ${msg}
                        <a href="/admin/${returnUrl}">返回列表</a>
                </div>
            </div>
        </section>
    </div>
</div>

<#include "script.ftl">
<script>
    $(function () {
        <#--function goto() {-->
            <#--window.location.href="http://localhost:8080/admin/${returnUrl}";-->
        <#--}-->
        <#--setTimeout(goto(),5000);-->
    })
</script>
</body>
</html>