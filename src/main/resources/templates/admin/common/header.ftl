<!--
  页面顶部栏
  -->

<!-- Page Header -->
  <header class="main-header">
      <!-- Logo -->
      <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>后台</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">博客后台管理</span>
      </a>

      <!-- NavBar -->
      <nav class="navbar navbar-static-top" role="navigation">
          <!-- Swtich NavBar Style-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
              <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Messages Nofications -->
          <div class="navbar-custom-menu">
              <ul class="nav navbar-nav">
                  <!-- /.messages-menu -->

                  <!-- User Account Menu -->
              </ul>
          </div>
      </nav>
  </header>