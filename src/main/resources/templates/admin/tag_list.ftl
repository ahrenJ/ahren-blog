<#-- 分类列表页 -->
<!DOCTYPE html>
<html>
<head>
    <#include "common/head.ftl">
    <title>${BLOG_NAME}-后台</title>
</head>
<body class="layout-boxed skin-black-light sidebar-mini">
<div class="wrapper">
    <!-- Page Header -->
    <#include "common/header.ftl">
    <!-- Left Sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- User Info Panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/static/AdminLTE-2.4.10/dist/img/owner.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>${OWNER_NAME}</p>
                    <a href="javascript:void(0)"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <ul class="sidebar-menu" data-widget="tree"><#-- 左侧导航栏 -->
                <li class="header">HEADER</li>
                <li><a href="/admin/"><i class="fa fa-home"></i> <span>主页</span></a></li>
                <li class="treeview active">
                    <a href="#"><i class="fa fa-book"></i> <span>文章</span>
                        <span class="pull-right-container">
				        <i class="fa fa-angle-left pull-right"></i>
				      </span>
                    </a>
                    <ul class="treeview-menu menu-open" style="display: block">
                        <li><a href="/admin/article/list"><i class="fa fa-circle-o"></i>文章列表</a></li>
                        <li><a href="/admin/article/post"><i class="fa fa-circle-o"></i>添加文章</a></li>
                        <li><a href="/admin/category/list"><i class="fa fa-circle-o"></i>分类</a></li>
                        <li class="active"><a href="/admin/tag/list"><i class="fa fa-circle-o"></i>标签</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-comments-o"></i> <span>评论</span>
                        <span class="pull-right-container">
				        <i class="fa fa-angle-left pull-right"></i>
				      </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/comment/list"><i class="fa fa-circle-o"></i>列表</a></li>
                    </ul>
                </li>
                <li><#-- 系统设置 -->
                    <a href="/admin/config"><i class="fa fa-cog"></i> <span>系统设置</span></a>
                </li>
                <li><a href="/"><i class="fa fa-hand-o-right"></i> <span>返回博客</span></a></li>
            </ul><#-- /.左侧导航栏 -->
        </section>
    </aside>
    <div class="content-wrapper">
        <section class="content-header"><#-- 路径 -->
            <h1>&nbsp;</h1>
            <ol class="breadcrumb">
                <li><a href="#">标签</a></li>
                <li class="active">列表</li>
            </ol>
        </section><#-- /.路径 -->
        <section class="content container-fluid"><#-- 右侧内容页 -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header">
                            <h3 class="box-title">标签列表</h3>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th width="150px">ID</th>
                                        <th width="200px">名称</th>
                                        <th width="300px">修改</th>
                                        <th width="80px"></th>
                                    </tr>
                                    <#list tagPageInfo.list  as tag>
                                    <tr style="padding-top: 3px">
                                        <td>${tag.id?c}</td>
                                        <td>${tag.name}</td>
                                        <td>
                                            <form id="update-form-${tag.id?c}" action="/admin/tag/update" method="post">
                                                <input name="tagId" type="hidden" value="${tag.id?c}">
                                                <input name='tagName' type='text' placeholder='修改名称为'>&nbsp;
                                                <input id='submit-update' type='button' class='btn btn-flat bg-gray-light' value='确认修改'>
                                            </form>
                                        </td>
                                        <td><a href="/admin/tag/${tag.id}/delete">删除</a></td>
                                    </tr>
                                    </#list>
                                </tbody>
                            </table>
                        </div>
                        <div class="box-body"><#-- 按钮 -->
                            <form id="add-form" action="/admin/tag/add" method="post">
                                <input id="open" type="button" value="添加标签" class="btn btn-flat btn-default">&nbsp;&nbsp;
                            </form>
                        </div><#-- /.按钮 -->
                        <div class="box-footer"><#-- 分页栏 -->
                            <div class="col-sm-6">
                                <div class="dataTables_paginate paging_simple_numbers">
                                    <ul class="pagination">
                                        <#if currentPage lte 1>
                                            <li class="paginate_button previous disabled"><a href="#">上一页</a></li>
                                        <#else>
                                            <li><a href="/admin/comment/tag?page=${currentPage - 1}&size=${size}">上一页</a></li>
                                        </#if>

                                        <#list 1..tagPageInfo.pages as index>
                                            <#if currentPage == index>
                                                <li class="paginate_button active"><a href="#">${index}</a></li>
                                            <#else>
                                                <li class="paginate_button"><a href="/admin/tag/list?page=${index}&size=${size}">${index}</a></li>
                                            </#if>
                                        </#list>

                                        <#if currentPage gte tagPageInfo.pages>
                                            <li class="paginate_button next disabled"><a href="#">下一页</a></li>
                                        <#else>
                                            <li class="paginate_button"><a href="/admin/tag/list?page=${currentPage + 1}&size=${size}">下一页</a>
                                            </li>
                                        </#if>
                                    </ul>
                                </div>
                            </div>
                        </div><#-- /.分页栏 -->
                    </div>
                </div>
        </section><#-- 右侧内容页 -->
    </div>
</div>
<#include "common/script.ftl">
<script>
    $(function () {
        //开启or关闭添加分类表单
        var opened=false;
        $("#open").on("click",function () {
            if (!opened){
                var html="<span>标签名：</span><input name='name' type='text' placeholder='请输入标签名'>&nbsp;"+
                        "<input id='add' type='button' class='btn btn-flat bg-gray-light' value='确认'>";
                //添加表单元素
                $("#add-form").append(html);
                opened=true;
            }else{
                $("#open").nextAll().remove();
                opened=false;
            }
        });
        //绑定提交表单事件
        $(document).on("click","#add", function () {
            var name = $("input[name='name']").val();
            if (name == null || name == "") {
                alert("标签名不能为空");
                return false;
            }
            $("#add-form").submit();
        });

        //绑定提交更新表单事件
        $("[id='submit-update']").on("click", function () {
            var tagName = $(this).prev().val();
            if (tagName == null || tagName == "") {
                alert("分类名不能为空");
                return false;
            }
            $(this).parent().submit();
        });
    })
</script>
</body>
</html>