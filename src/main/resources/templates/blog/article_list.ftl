<!DOCTYPE html>
<html>
<head>
    <title>文章列表 - ${BLOG_NAME}</title>
    <#include "common/head.ftl">
    <link rel="stylesheet" href="/static/css/pagination.css">
</head>
<body class="layout-top-nav skin-black-light">
<div class="wrapper"><#-- Page Wrapper -->
    <div class="content-wrapper" style="background-color: #2c3b41"><#-- Content Wrapper -->
        <div class="container" style="width: 1080px;"><#-- Content Container -->
            <section class="content"><#-- Main Content -->
                <div class="row"><#-- 顶部标题LOG -->
                    <#include "common/top.ftl">
                </div>
                <div class="row">
                    <div class="col-md-3"><#-- 左侧侧边栏 -->
                        <#include "common/sidebar.ftl">
                    </div><#-- /.左侧侧边栏 -->
                    <div class="col-md-9">
                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <span class="text-bold"><i class="fa fa-align-justify"></i>&nbsp;&nbsp;Article List</span>
                            </div>
                            <!-- /.box-header -->
                            <div id="title-list-container">
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <div id="pagination" style="margin-left: auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <#include "common/footer.ftl">
            </section>
        </div><#-- /.container -->
    </div><#-- /.content-wrapper -->
</div><#-- /.wrapper -->
<#include "../admin/common/script.ftl">
<script src="/static/js/pagination.min.js"></script>
<script>
    $(function () {
        //初始化分页
        var myPagination=$("#pagination");
        myPagination.pagination({
            dataSource: '${dataUrl}',
            totalNumber: ${totalNumber},
            locator:'data',
            pageSize:10,
            callback: function (data,pagination) {
                $("#title-list-container").empty();
                var html=renderHtml(data);
                $("#title-list-container").append(html);
            },
            alias:{ pageNumber:'page' },
            showPrevious:true,
            showNext:true,
            className: 'paginationjs-small'
        });

        //渲染列表页Html
        function renderHtml(data) {
            var html="";
            $.each(data,function (index,articleVo) {
                html=html+"<div class='box-header with-border'>" +
                        "<a href='/article/"+articleVo.id+"'><h4 class='box-title'>"+
                        "<i class='fa fa-angle-double-right'></i>&nbsp;" +
                        "<span class='text-muted'>["+articleVo.categoryName+"] </span>"+articleVo.title +"</h4></a>"+
                        "<span class='description text-muted pull-right'>发布于 "+articleVo.createTime+"</span>"+
                        "</div>";
            })
            return html;
        }
    })
</script>
</body>
</html>
