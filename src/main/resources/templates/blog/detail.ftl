<!DOCTYPE html>
<html>
<head>
    <meta name="referrer" content="no-referrer"/>
    <link rel="stylesheet" href="/static/AdminLTE-2.4.10/bower_components/editor.md-master/css/editormd.preview.css" />
    <title>${articleDto.getTitle()} - ${BLOG_NAME}</title>
    <#include "common/head.ftl">
    <link rel="stylesheet" href="/static/css/pagination.css">
</head>
<body class="layout-top-nav skin-black-light">
<div class="wrapper"><#-- Page Wrapper -->
    <div class="content-wrapper" style="background-color: #2c3b41"><#-- Content Wrapper -->
        <div class="container" style="width: 1080px;"><#-- Content Container -->
            <section class="content"><#-- Main Content -->
                <div class="row"><#-- 顶部标题LOG -->
                    <#include "common/top.ftl">
                </div>
                <div class="row">
                    <div class="col-md-3"><#-- 左侧侧边栏 -->
                        <#include "common/sidebar.ftl">
                    </div><#-- /.左侧侧边栏 -->
                    <div class="col-md-9">
                        <div class="box box-solid">
                            <div class="box-header text-center with-border"><#-- 文章标题栏 -->
                                <h2>${articleDto.getTitle()}</h2>
                                <span class="description text-muted">
                                    发布于 ${articleDto.getCreateTime()?string("yyyy-MM-dd")}&nbsp;|&nbsp;
                                    <span class="glyphicon glyphicon-eye-open"></span>&nbsp;阅读数&nbsp;${articleDto.readNum}&nbsp;|&nbsp;
                                    <span class="glyphicon glyphicon-heart"></span>&nbsp;点赞数&nbsp;${articleDto.likerNum}
                                </span>
                            </div><#-- /.文章标题栏 -->
                            <div class="box-body">
                                <div id="editor-text"><#-- 文章内容 -->
                                    <textarea style="display: none">${articleDto.getContent()}</textarea>
                                </div><#-- /.文章内容 -->
                                <span class="text-muted pull-left"><#-- 文章页脚标签 -->
                                    标签：
                                    <#if articleDto.tags?? && (articleDto.tags?size>0)>
                                        <#list articleDto.tags as tag>
                                        <a href="/article/list?tagName=${tag.name}" target="_blank" class="text-muted">${tag.name}&nbsp;</a>
                                        </#list>
                                    </#if>
                                </span><#-- /.文章页脚标签 -->
                                <span class="text-muted pull-right"><#-- 文章页脚（点赞、评论） -->
                                    <a id="like-action" href="javascript:void(0)">点赞
                                        <#-- 判断当前游客是否已对该文点过赞 -->
                                        <#if hasLike==false>
                                        <i class="fa fa-heart-o"></i>
                                        <#else>
                                        <i class="fa fa-heart"></i>
                                        </#if>
                                    </a>
                                    <span>(${articleDto.getLikerNum()}) &nbsp;&nbsp;&nbsp; 评论(${articleDto.getCommentNum()})</span>
                                </span><#-- /.文章页脚（点赞、评论） -->
                            </div>
                            <div class="box-footer"><#-- 评论列表 -->
                                <#list commentPageInfo.getList() as comment>
                                    <div class="post"><#-- 评论 -->
                                        <div class="user-block">
                                            <img class="img-circle img-sm" src="/static/AdminLTE-2.4.10/dist/img/user.jpg" alt="User Image">
                                            <span class="username"><a href="javascript:void(0)" class="link-muted">${comment.userName}</a></span><!-- /.username -->
                                        </div>
                                        <p>${comment.content}</p>
                                        <ul class="list-inline">
                                            <li></li>
                                            <li class="pull-right">
                                                <span class="text-muted">${comment.createTime?string("yyyy-MM-dd HH:mm")}</span>
                                            </li>
                                            <li class="pull-right"><#-- 评论回复数 -->
                                                <#-- 展开评论 -->
                                                <a id="showReply${comment.id}" href="javascript:void(0)" class="text-sm">
                                                    <span class="text-muted text-bold">#${comment_index+1}&nbsp;&nbsp;</span>
                                                    <i class="fa fa-comments-o margin-r-5"></i>
                                                    <span>回复(${comment.replyNum})</span>
                                                </a>
                                            </li>
                                            <script>
                                                $(function () {
                                                    $("#showReply${comment.id}").on("click",function () {
                                                        var show=$(this).parent().parent().next();
                                                        if (show.is(":hidden")){
                                                            show.show();

                                                            //初始化分页
                                                            var myPagination=$("#pagination-${comment.id}");
                                                            myPagination.pagination({
                                                                dataSource: function (done) {
                                                                    $.ajax({
                                                                        type:"GET",
                                                                        contentType:"application/json",
                                                                        url:"/ajax/replyList/${comment.id}",
                                                                        success:function (data) {
                                                                            done(data);
                                                                        }
                                                                    });
                                                                },
                                                                callback:function(data,pagination){
                                                                    $("#reply-list-${comment.id}").empty();
                                                                    var html="";
                                                                    $.each(data,function (index,reply) {
                                                                        html=html+"<div class='box-comment'>"+
                                                                                      "<img class='img-circle img-sm' src='/static/AdminLTE-2.4.10/dist/img/user.jpg' alt='User Image'>"+
                                                                                      "<div class='comment-text'>"+
                                                                                          "<span class='username'>"+reply.userName+"</span>"+reply.content+"<br>" +
                                                                                          "<span class='text-muted pull-right'>"+reply.createTime+"&nbsp;&nbsp;"+
                                                                                          "<a id='reply-"+reply.id+"' href='javascript:void(0)'>&nbsp;回复</a>"+
                                                                                          "</span>"+
                                                                                      "</div>"+
                                                                                  "</div>";
                                                                        $("body").on("click","#reply-"+reply.id,function () {
                                                                            $("#reply-form-${comment.id} input[name='rp-content']").val("回复 @"+reply.userName+": ");
                                                                            $("#reply-form-${comment.id} input[name='rp-content']").before("<input name='rp-toId' type='hidden' value='"+reply.id+"'>");
                                                                        });
                                                                    });
                                                                    $("#reply-list-${comment.id}").append(html);
                                                                },
                                                                pageSize:5,
                                                                alias:{ pageNumber:'page' },
                                                                showPrevious:true,
                                                                showNext:true,
                                                                className: 'paginationjs-small'
                                                            });
                                                        }else{
                                                            show.hide();
                                                        }
                                                    });
                                                })
                                            </script>
                                        </ul>
                                        <div class="box box-widget" style="display:none;border: #c5c5c5 1px solid;"><#-- 回复列表 -->
                                            <div id="reply-list-${comment.id}" class="box-footer box-comments">
                                                <#--回复列表内容-->
                                            </div>
                                            <div class="box-footer"><#-- 分页栏 -->
                                                <div id="pagination-${comment.id}" class="pull-right"></div>
                                            </div><#-- /.分页栏 -->
                                            <div class="box-footer"><#-- 发表回复 -->
                                                <form action="#" method="post" id="reply-form-${comment.id}">
                                                    <img class="img-responsive img-circle img-sm" src="/static/AdminLTE-2.4.10/dist/img/user.jpg" alt="Alt Text">
                                                    <!-- .img-push is used to add margin to elements next to floating images -->
                                                    <div class="img-push">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-user"></i></span><input name="rp-name" type="text" class="form-control input-sm`" placeholder="用户名" style="width:200px">
                                                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span><input name="rp-email" type="text" class="form-control input-sm`" placeholder="邮箱(非必填)" style="width:200px;margin-right: 130px">
                                                        </div>
                                                        <div class="input-group input-group-sm" style="margin-top: 5px">
                                                            <input name="rp-content" type="text" class="form-control" placeholder="回复内容...">
                                                            <span class="input-group-btn">
                                                                <button id="replyTo${comment.id}" type="button" class="btn btn-info btn-flat">回复</button>
                                                                <script>
                                                                    $(function () {
                                                                        var curReplyNum=${comment.replyNum};
                                                                        $("#replyTo${comment.id}").on("click",function () {
                                                                            var toId=$("#reply-form-${comment.id} input[name='rp-toId']").val();
                                                                            if (toId==null)
                                                                                toId=-1;
                                                                            var userName=$("#reply-form-${comment.id} input[name='rp-name']").val();
                                                                            var userEmail=$("#reply-form-${comment.id} input[name='rp-email']").val();
                                                                            var content=$("#reply-form-${comment.id} input[name='rp-content']").val();
                                                                            $.ajax({
                                                                                type:"POST",
                                                                                url:"/ajax/reply",
                                                                                contentType:"application/json",
                                                                                data: JSON.stringify({
                                                                                    "toId":toId,
                                                                                    "userName":userName,
                                                                                    "userEmail":userEmail,
                                                                                    "content":content,
                                                                                    "commentId":"${comment.id}"
                                                                                }),
                                                                                success:function (data) {
                                                                                    curReplyNum++;
                                                                                    alert("回复成功");
                                                                                    $("#showReply${comment.id}").children().next().next().text("");
                                                                                    $("#showReply${comment.id}").children().next().next().text("回复("+(curReplyNum)+")");
                                                                                    $("#reply-form-${comment.id} input[name='rp-content']").val("");
                                                                                    $("#showReply${comment.id}").click();
                                                                                    $("#showReply${comment.id}").click();
                                                                                }
                                                                            });
                                                                        });
                                                                    })
                                                                </script>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div><#-- /.发表回复 -->
                                        </div><#-- /.回复列表 -->
                                    </div><#-- /.评论 -->
                                </#list>
                            </div><#-- /.评论列表 -->
                            <div class="box-footer"><#-- 发表评论表单 -->
                                <form action="/comment/post" method="post" role="form">
                                    <img class="img-responsive img-circle img-sm" src="/static/AdminLTE-2.4.10/dist/img/user.jpg" alt="Alt Text">
                                    <div class="img-push">
                                        <input name="articleId" type="hidden" value="${articleDto.getId()}">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            <input name="userName" type="text" required="required" class="form-control" placeholder="用户名(随意)" style="width: 30%;">
                                        </div>
                                        <div class="input-group" style="margin-top: 7px">
                                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                            <input name="userEmail" type="email" class="form-control" placeholder="邮箱(非必填)" style="width: 40%;">
                                        </div>
                                        <textarea name="content" class="form-control" required="required" placeholder="内容..." style="height: 100px;margin-top: 7px"></textarea>
                                        <div class="box-footer">
                                            <button id="commit" type="button" class="btn btn-default pull-right"> 提交</button>
                                        </div>
                                    </div>
                                </form>
                            </div><#-- /.发表评论表单 -->
                        </div>
                    </div>
                </div>
                <#include "common/footer.ftl">

            </section>
        </div><#-- /.container -->
    </div><#-- /.content-wrapper -->
</div><#-- /.wrapper -->
<#include "../admin/common/script.ftl">
<script src="/static/AdminLTE-2.4.10/bower_components/editor.md-master/lib/marked.min.js"></script>
<script src="/static/AdminLTE-2.4.10/bower_components/editor.md-master/lib/prettify.min.js"></script>
<script src="/static/AdminLTE-2.4.10/bower_components/editor.md-master/lib/raphael.min.js"></script>
<script src="/static/AdminLTE-2.4.10/bower_components/editor.md-master/lib/underscore.min.js"></script>
<script src="/static/AdminLTE-2.4.10/bower_components/editor.md-master/lib/sequence-diagram.min.js"></script>
<script src="/static/AdminLTE-2.4.10/bower_components/editor.md-master/lib/flowchart.min.js"></script>
<script src="/static/AdminLTE-2.4.10/bower_components/editor.md-master/lib/jquery.flowchart.min.js"></script>
<script src="/static/AdminLTE-2.4.10/bower_components/editor.md-master/editormd.min.js"></script>
<script src="/static/js/pagination.min.js"></script>
<script>
    $(function () {
        //评论表单-检验并提交
        var flag1=false;
        var flag2=false;
        var flag3=false;
        $("#commit").on("click",function () {
            var userName=$("input[name='userName']").val();
            var userEmail=$("input[name='userEmail']").val();
            var content=$("textarea[name='content']").val();
            if (userName==null||userName==""){
                if (!flag1){
                    $("input[name='userName']").after("<span id='null-name' class='text-red'>*用户名不能为空</span>");
                    flag1=true;
                }
                return;
            }
            if (content==null||content==""){
                if (!flag2){
                    $("textarea[name='content']").after("<span id='null-content' class='text-red'>*评论内容不能为空</span>");
                    flag2=true;
                }
                return;
            }
            if(userEmail!=null && userEmail!="" && !userEmail.match(/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/)) {
                if (!flag3){
                    $("input[name='userEmail']").after("<span id='email-error' class='text-red'>*邮箱格式不正确</span>");
                    flag3=true;
                }
                return;
            }
            $("form").submit();
        });

        //userName输入框onChange事件
        $("input[name='userName']").on("change",function () {
            $("#null-name").remove();
        })
        //content输入框onChange事件
        $("textarea[name='content']").on("change",function () {
            $("#null-content").remove();;
        })
        //userEmail输入框onChange事件
        $("input[name='userEmail']").on("change",function () {
           $("#email-error").remove();
        });

        //editor.md文章内容加载
        var editorText = editormd.markdownToHTML("editor-text",{
            htmlDecode:true,
            htmlDecode: "style,script,iframe",
            tocm: true,
            emoji: true,
            taskList: true,
            markdownSourceCode: true,
            tex: true,
            flowChart: true,
            sequenceDiagram: true,
            hideWhenLessThanOnePage:true
        });

        var curLikerNum=${articleDto.getLikerNum()};
        //点赞事件
        $("#like-action").on("click",function () {
            var curCommentNum=${articleDto.getCommentNum()};
            var t=$(this);
            $.ajax({
                url:"/ajax/article/like",
                type:"POST",
                contentType:"application/x-www-form-urlencoded",
                data:{
                    articleId:"${articleDto.id}"
                },
                success:function (data) {
                    if (data=="asc"){
                        curLikerNum++;
                        t.children("i").attr("class","fa fa-heart");
                        t.next().html("("+(curLikerNum)+") &nbsp;&nbsp;&nbsp; 评论(${articleDto.getCommentNum()})");
                    }else if(data=="desc"){
                        curLikerNum--;
                        t.children("i").attr("class","fa fa-heart-o");
                        t.next().html("("+(curLikerNum)+") &nbsp;&nbsp;&nbsp; 评论(${articleDto.getCommentNum()})");
                    }
                }
            })
        });
    })
</script>
</body>
</html>
