<#--顶部标题LOGO-->
<div class="col-md-12">
    <div class="box box-widget widget-user">
        <div class="widget-user-header bg-black" style="background: url('/static/AdminLTE-2.4.10/dist/img/photo1.png')" >
            <h2 class="box-title">${OWNER_NAME?cap_first}'s&nbsp;&nbsp;Personal&nbsp;&nbsp;Blog</h2>
            <h5 class="widget-user-desc">&nbsp;&nbsp;&nbsp;&mdash; Record my program development experience</h5>
        </div>
    </div>
</div>