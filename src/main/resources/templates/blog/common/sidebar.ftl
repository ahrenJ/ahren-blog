<div class="box box-widget widget-user-2"><#-- 博主信息 -->
    <div class="widget-user-header bg-black">
        <div class="widget-user-image">
            <img class="img-circle img-bordered-sm" src="/static/AdminLTE-2.4.10/dist/img/owner.jpg" alt="User Avatar">
        </div>
        <h6 class="widget-user-username">${OWNER_NAME}</h6>
        <h6 class="widget-user-desc">Java Developer</h6>
    </div>
    <div class="box-footer no-padding">
        <ul class="nav nav-stacked">
            <li><a href="/"><i class="fa fa-home"></i>&nbsp;&nbsp;首页</a></li>
            <li><a href="/article/list"><i class="fa fa-book"></i>&nbsp;&nbsp;文章<span id="total" class="pull-right badge bg-black"></span></a></li>
            <li><a href="http://github.com/ahrenJ" target="_blank"><i class="fa fa-github-alt"></i>&nbsp;&nbsp;GitHub</a></li>
            <li><a href="javascript:void(0)"><i class="fa fa-envelope"></i>&nbsp;&nbsp;${OWNER_EMAIL}</a></li>
            <form action="/article/list" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="title" class="form-control" placeholder="查询文章...">
                    <span class="input-group-btn">
                        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>
        </ul>
    </div>
</div><#-- /.博主信息 -->
<div class="box box-solid" style="margin-top: 20px"><#-- /.标签栏 -->
    <div id="tagbar" class="box-body">
        <strong><i class="fa fa-tags"></i> 标签</strong><br>
    </div>
</div><#-- /.标签栏 -->
<div class="box box-solid" style="margin-top: 20px"><#-- 分类栏 -->
    <div class="box-body">
        <strong><i class="fa fa-tags"></i> 分类</strong><br>
        <ul id="category-list" class="list-unstyled">

        </ul>
    </div>
</div><#-- /.分类栏 -->
<script src="/static/AdminLTE-2.4.10/bower_components/jquery/dist/jquery.min.js"></script>
<script>
    $(function () {
        //异步渲染侧边文章标签栏
        $.get("/ajax/getTags",function (data) {
            var tagbarHtml="<p id='tags'>\r\n";
            $.each(data,function (index,tag) {
                var labelArr=["label-danger","label-success","label-info","label-warning","label-primary"];
                tagbarHtml=tagbarHtml+"<a href='/article/list?tagName="+tag.name+"'>" +
                                          "<span id='tag"+index+"' class='label "+labelArr[index%labelArr.length]+"'>" +
                                                "&nbsp;"+tag.name +"&nbsp;"+
                                          "</span>" +
                                      "</a>\r\n";
            });
            tagbarHtml=tagbarHtml+"</p>";
            $("#tagbar").append(tagbarHtml);
        });

        //异步获取文章总数
        $.get("/ajax/article/total",function (data) {
            $("#total").text(data+"篇");
        });

        //异步获取分类和文章数量汇总信息
        $.get("/ajax/category/list",function (data) {
            var html="";
            $.each(data,function (index,cateInfo) {
                html=html+"<li><i class='fa fa-genderless'></i>&nbsp;" +
                              "<a href='/article/list?categoryId="+cateInfo.id+"' style='text-decoration: underline'>" +
                                  "<span class='text-muted'>"+cateInfo.name+"</span>&nbsp;" +
                                  "<span class='text-blue text-bold'>("+cateInfo.articleNum+")</span>" +
                              "</a>" +
                          "</li>";
            })
            $("#category-list").append(html);
        })
    })
</script>