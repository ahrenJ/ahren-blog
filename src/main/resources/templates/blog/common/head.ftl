<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="/static/AdminLTE-2.4.10/bower_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="/static/AdminLTE-2.4.10/bower_components/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/static/AdminLTE-2.4.10/bower_components/Ionicons/css/ionicons.min.css">
<link rel="stylesheet" href="/static/AdminLTE-2.4.10/dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="/static/AdminLTE-2.4.10/dist/css/skins/skin-black-light.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link rel="icon" type="image/x-icon" href="/static/AdminLTE-2.4.10/dist/img/web-icon.ico"/>