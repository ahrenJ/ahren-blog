<!DOCTYPE html>
<html>
<head>
    <title>${BLOG_NAME}</title>
    <#include "common/head.ftl">
</head>
<body class="layout-top-nav skin-black-light">
<div class="wrapper"><#-- Page Wrapper -->
    <div class="content-wrapper" style="background-color: #2c3b41"><#-- Content Wrapper -->
        <div class="container" style="width:1080px;"><#-- Content Container -->
            <section class="content"><#-- Main Content -->
                <div class="row"><#-- 顶部标题LOG -->
                    <#include "common/top.ftl">
                </div>
                <div class="row">
                    <div class="col-md-3"><#-- 左侧侧边栏 -->
                        <#include "common/sidebar.ftl">
                    </div><#-- /.左侧侧边栏 -->
                    <div class="col-md-9"><#-- 右侧主要内容 -->
                        <div class="box box-widget widget-user" style="height: 35px;margin-bottom: 20px">
                            <div class="widget-user-header bg-black">
                                <span class="text-bold"><i class="fa fa-align-justify"></i>&nbsp;&nbsp;Lasted Updated</span>
                            </div>
                        </div>
                        <#list articleDtos as articleDto>
                        <div class="box box-widget"><#-- 最新文章列表 -->
                            <div class="box-header with-border">
                                <h3 class="box-title">
                                    <span class="text-muted">[${articleDto.categoryName}] </span>
                                    <a href="/article/${articleDto.getId()}">${articleDto.title}</a>
                                </h3>
                                <span class="description text-muted pull-right">发布于 ${articleDto.createTime?string("yyyy-MM-dd")}</span>
                            </div>
                            <div class="box-body">
                                <p><span class="text-muted"><cite title="Source Title">${articleDto.summary}</cite></span></p>
                                <i class="fa fa-tags">&nbsp;</i>
                                <span id="article-tags">
                                    <#if articleDto.tags?? && (articleDto.tags?size>0)>
                                        <#list articleDto.tags as tag>
                                        <a href="/article/list?tagName=${tag.name}" class="text-muted">${tag.name}&nbsp;</a>
                                        </#list>
                                    </#if>
                                </span>
                                <span class="pull-right text-muted">
                                    <span><i class="fa fa-heart"></i>点赞(${articleDto.likerNum})</span>&nbsp;&nbsp;&nbsp;
                                    <span><i class="fa fa-comment-o"></i>&nbsp;评论(${articleDto.commentNum})</span>
                                </span>
                            </div>
                        </div><#-- /.最新文章列表 -->
                        </#list>
                        <div class="box box-widget"><#-- 最新文章列表 -->
                            <div class="box-header with-border">
                                <h3 class="box-title">
                                    <a href="/article/list"><small class="text-blue">查看更多&nbsp;<i class="fa fa-angle-double-right"></i></small></a>
                                </h3>
                            </div>
                        </div>
                    </div><#-- /.右侧主要内容 -->
                </div><#-- /.row -->
                <#include "common/footer.ftl">
            </section>
        </div><#-- /.container -->
    </div><#-- /.content-wrapper -->
</div><#-- /.wrapper -->
<#include "../admin/common/script.ftl">
</body>
</html>
