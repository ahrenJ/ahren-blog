package cn.ahren.blog.service;

import cn.ahren.blog.entity.Reply;
import com.github.pagehelper.PageInfo;

//评论回复服务
public interface ReplyService {

    //查询某个评论的所有回复
    PageInfo<Reply> findReplyByCommentId(Integer commentId, Integer page, Integer pageSize);

    //回复评论
    int replyToComment(Reply reply);

    //删除回复
    int delete(Integer id,Integer commentId);

    //删除某个评论的所有回复
    int deleteByCommentId(Integer commentId);
}
