package cn.ahren.blog.service;

import cn.ahren.blog.entity.Comment;
import cn.ahren.blog.form.CommentForm;
import com.github.pagehelper.PageInfo;

//文章评论服务
public interface CommentService {

    //查询某条评论
    Comment findById(Integer id);

    //查询所有评论（后台）
    PageInfo<Comment> findAll(Integer page,Integer pageSize);

    //查询某篇文章的所有评论
    PageInfo<Comment> findByArticleId(String articleId,Integer page,Integer pageSize);

    //发表评论
    int comment(CommentForm commentForm)throws Exception;

    //删除评论
    int delete(Integer id,String articleId)throws Exception;
}
