package cn.ahren.blog.service;


import cn.ahren.blog.entity.Tag;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface TagService {

    //查询所有标签
    List<Tag> findAll();

    //分页查询所有标签
    PageInfo<Tag> findAll(Integer page,Integer pageSize);

    //查询标签-根据标签ID集合
    List<Tag> findByIdList(List<Integer> idList);

    //添加标签
    int add(Tag tag);

    //更新标签
    int update(Tag tag);

    //删除标签
    int delete(Integer id);
}
