package cn.ahren.blog.service;

import cn.ahren.blog.entity.Category;
import cn.ahren.blog.vo.CateToArticleNumVo;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface CategoryService {

    //分页查询所有分类
    PageInfo<Category> findAll(Integer page,Integer pageSize);

    //查询分类以及分类对应的文章数量
    List<CateToArticleNumVo> findAndCountArticleNum();

    //查询所有分类
    List<Category> findAll();

    //添加分类
    int add(Category category);

    //更新分类
    int update(Category category);

    //删除分类-根据ID
    int delete(Integer id);
}
