package cn.ahren.blog.service.impl;

import cn.ahren.blog.dao.CommentDao;
import cn.ahren.blog.dao.ReplyDao;
import cn.ahren.blog.entity.Reply;
import cn.ahren.blog.enums.BlogErrorEnum;
import cn.ahren.blog.exception.BlogException;
import cn.ahren.blog.service.ReplyService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Slf4j
public class ReplyServiceImpl implements ReplyService {

    @Autowired
    private ReplyDao replyDao;
    @Autowired
    private CommentDao commentDao;

    //查找某条评论的所有回复
    @Override
    public PageInfo<Reply> findReplyByCommentId(Integer commentId, Integer page, Integer pageSize) {
        PageHelper.startPage(page,pageSize,"create_time ASC");
        List<Reply> replies=replyDao.selectByCommentId(commentId);
        PageInfo<Reply> pageInfo=new PageInfo<>(replies);
        return pageInfo;
    }

    //回复评论
    @Override
    public int replyToComment(Reply reply) {
        int result=replyDao.insert(reply);
        commentDao.incReplyNum(reply.getCommentId());
        if (result==0){
            log.error("[Frontend.Blog-回复服务],错误：{}",BlogErrorEnum.REPLY_POST_ERROR.getMsg());
            throw new BlogException(BlogErrorEnum.REPLY_POST_ERROR);
        }
        log.info("[Frontend.Blog-回复服务]：回复评论-{}",reply);
        return result;
    }

    //删除回复
    @Override
    public int delete(Integer id,Integer commentId) {
        int result=replyDao.delete(id);
        commentDao.decReplyNum(commentId);
        if (result==0){
            log.error("[Admin-回复服务]：错误-{}",BlogErrorEnum.REPLY_NO_EXIST.getMsg());
            throw new BlogException(BlogErrorEnum.REPLY_NO_EXIST);
        }
        log.info("[Admin-回复服务]：删除回复-{}",id);
        return result;
    }

    //删除某条评论下的所有回复
    @Override
    public int deleteByCommentId(Integer commentId) {
        int result=replyDao.deleteByCommentId(commentId);
        log.info("[Admin-回复服务]：删除该评论的回复-{}",commentId);
        return result;
    }

}
