package cn.ahren.blog.service.impl;

import cn.ahren.blog.dao.CommentDao;
import cn.ahren.blog.entity.Comment;
import cn.ahren.blog.enums.BlogErrorEnum;
import cn.ahren.blog.exception.BlogException;
import cn.ahren.blog.form.CommentForm;
import cn.ahren.blog.service.ArticleService;
import cn.ahren.blog.service.CommentService;
import cn.ahren.blog.service.ReplyService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
@Slf4j
public class CommentServiceImpl implements CommentService {

    @Autowired
    private ArticleService articleService;
    @Autowired
    private CommentDao commentDao;
    @Autowired
    private ReplyService replyService;

    //查询评论详情
    @Override
    public Comment findById(Integer id) {
        Comment comment=commentDao.selectById(id);
        if (ObjectUtils.isEmpty(comment)){
            log.info("[Admin-评论服务]-错误：{}",BlogErrorEnum.COMMENT_NO_EXIST.getMsg());
            throw new BlogException(BlogErrorEnum.COMMENT_NO_EXIST);
        }
        return comment;
    }

    //查询所有文章评论
    @Override
    public PageInfo<Comment> findAll(Integer page,Integer pageSize) {
        PageHelper.startPage(page,pageSize,"aid");
        List<Comment> comments=commentDao.selectAll();
        PageInfo<Comment> pageInfo=new PageInfo<>(comments);
        return pageInfo;
    }

    //查询某篇文章的所有评论
    @Override
    public PageInfo<Comment> findByArticleId(String articleId, Integer page, Integer pageSize) {
        PageHelper.startPage(page,pageSize,"create_time DESC");
        List<Comment> comments=commentDao.selectByArticleId(articleId);
        PageInfo<Comment> pageInfo=new PageInfo<>(comments);
        return pageInfo;
    }

    //发表评论
    @Override
    @Transactional
    public int comment(CommentForm commentForm)throws Exception {
        Comment comment=new Comment();
        BeanUtils.copyProperties(commentForm,comment);

        //评论入库
        int result=commentDao.insert(comment);
        if (result==0){
            log.error("[Frontend.Blog-评论服务]：错误：{}",BlogErrorEnum.COMMENT_POST_ERROR.getMsg());
            throw new BlogException(BlogErrorEnum.COMMENT_POST_ERROR);
        }
        //文章增加评论数
        articleService.updateCommentNum(commentForm.getArticleId(),true);
        log.info("[Frontend.Blog-评论服务]：发表评论-{}",commentForm);
        return result;
    }

    //删除评论
    @Override
    @Transactional
    public int delete(Integer id,String articleId) throws Exception{
        //删除评论
        int result=commentDao.delete(id);
        if (result==0){
            log.error("[Admin-评论服务]：错误：{}",BlogErrorEnum.COMMENT_NO_EXIST.getMsg());
            throw new BlogException(BlogErrorEnum.COMMENT_NO_EXIST);
        }
        //文章减少评论数
        articleService.updateCommentNum(articleId,false);
        //删除该评论的回复
        replyService.deleteByCommentId(id);
        log.info("[Admin-评论服务]：删除评论：{}",id);
        return result;
    }
}
