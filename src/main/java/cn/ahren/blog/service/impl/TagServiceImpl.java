package cn.ahren.blog.service.impl;

import cn.ahren.blog.dao.TagDao;
import cn.ahren.blog.entity.Tag;
import cn.ahren.blog.enums.BlogErrorEnum;
import cn.ahren.blog.exception.BlogException;
import cn.ahren.blog.service.TagService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
@Slf4j
public class TagServiceImpl implements TagService {

    @Autowired
    private TagDao tagDao;

    @Override
    public List<Tag> findAll() {
        return tagDao.selectAll();
    }

    @Override
    public PageInfo<Tag> findAll(Integer page, Integer pageSize) {
        PageHelper.startPage(page,pageSize,"id ASC");
        List<Tag> tags=tagDao.selectAll();
        PageInfo<Tag> pageInfo=new PageInfo<>(tags);
        return pageInfo;
    }

    //查找标签-根据ID集合
    @Override
    public List<Tag> findByIdList(List<Integer> idList) {
        if (!CollectionUtils.isEmpty(idList)){
            return tagDao.selectByIdList(idList);
        }
        throw new BlogException(BlogErrorEnum.TAG_NO_EXIST);
    }

    //添加标签
    @Override
    public int add(Tag tag) {
        int result=tagDao.insert(tag);
        if (result>=1){
            log.info("[Admin-文章标签服务]：添加标签-{}",tag);
            return result;
        }
        log.error("[Admin-文章标签服务]：错误-{}",BlogErrorEnum.TAG_ADD_ERROR.getMsg());
        return result;
    }

    //更新标签
    public int update(Tag tag){
        int result=tagDao.update(tag);
        if (result>=1){
            log.info("[Admin-文章标签服务]：更新标签-{}",tag);
            return result;
        }
        log.error("[Admin-文章标签服务]：错误-{}",BlogErrorEnum.TAG_UPDATE_ERROR.getMsg());
        throw new BlogException(BlogErrorEnum.TAG_UPDATE_ERROR);
    }

    //删除标签
    public int delete(Integer id){
        int result=tagDao.delete(id);
        if (result>=1){
            log.info("[Admin-文章标签服务]：删除标签-{}",id);
            return result;
        }
        log.error("[Admin-文章标签服务]：错误-{}",BlogErrorEnum.TAG_NO_EXIST.getMsg());
        throw new BlogException(BlogErrorEnum.TAG_NO_EXIST);
    }
}
