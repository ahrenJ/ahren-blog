package cn.ahren.blog.service.impl;

import cn.ahren.blog.dao.CategoryDao;
import cn.ahren.blog.entity.Category;
import cn.ahren.blog.enums.BlogErrorEnum;
import cn.ahren.blog.exception.BlogException;
import cn.ahren.blog.service.CategoryService;
import cn.ahren.blog.vo.CateToArticleNumVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryDao categoryDao;

    //分页查找
    @Override
    public PageInfo<Category> findAll(Integer page, Integer pageSize) {
        PageHelper.startPage(page,pageSize);
        List<Category> categories=categoryDao.selectAll();
        PageInfo<Category> pageInfo=new PageInfo<>(categories);
        return pageInfo;
    }

    //查询分类以及分类对应的文章数量
    @Override
    public List<CateToArticleNumVo> findAndCountArticleNum() {
        return categoryDao.countByArticleNum();
    }

    //查询所有类别
    @Override
    public List<Category> findAll() {
        List<Category> categories=categoryDao.selectAll() ;
        return categories;
    }

    //添加分类
    @Override
    public int add(Category category) {
        int result=categoryDao.insert(category);
        if (result>=1){
            log.info("[Admin-文章分类服务]：添加分类-{}",category);
            return result;
        }
        log.error("[Admin-文章分类服务]：错误-{}",BlogErrorEnum.CATEGORY_ADD_ERROR.getMsg());
        throw new BlogException(BlogErrorEnum.CATEGORY_ADD_ERROR);
    }

    //更新分类
    @Override
    public int update(Category category) {
        int result= categoryDao.update(category);
        if (result>=1){
            log.info("[Admin-文章分类服务]：更新分类-{}",category);
            return result;
        }
        log.error("[Admin-文章分类服务]：错误-{}",BlogErrorEnum.CATEGORY_UPDATE_ERROR.getMsg());
        throw new BlogException(BlogErrorEnum.CATEGORY_UPDATE_ERROR);
    }

    //删除分类
    @Override
    public int delete(Integer id) {
        int result= categoryDao.delete(id);
        if (result>=1){
            log.info("[Admin-文章分类服务]：删除分类-{}",id);
            return result;
        }
        log.error("[Admin-文章分类服务]：错误-{}",BlogErrorEnum.CATEGORY_NO_EXIST.getMsg());
        throw new BlogException(BlogErrorEnum.CATEGORY_NO_EXIST);
    }

}
