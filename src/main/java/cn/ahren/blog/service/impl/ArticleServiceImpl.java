package cn.ahren.blog.service.impl;

import cn.ahren.blog.dao.*;
import cn.ahren.blog.dto.ArticleDto;
import cn.ahren.blog.entity.Article;
import cn.ahren.blog.entity.ArticleTags;
import cn.ahren.blog.entity.Category;
import cn.ahren.blog.entity.Tag;
import cn.ahren.blog.enums.BlogErrorEnum;
import cn.ahren.blog.exception.BlogException;
import cn.ahren.blog.exception.BlogException;
import cn.ahren.blog.service.ArticleService;
import cn.ahren.blog.utils.ArticleConvertor;
import cn.ahren.blog.utils.KeyGenerator;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

//文章服务接口实现
@Slf4j
@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleDao articleDao;
    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private TagDao tagDao;
    @Autowired
    private ArticleTagsDao articleTagsDao;
    //由于分页查询问题，在dao层直接拼装ArticleDto
    @Autowired
    private ArticleDtoDao articleDtoDao;

    //添加文章
    @Transactional
    public int add(ArticleDto articleDto) throws Exception {
        //文章入库
        Article article = ArticleConvertor.convertToArticle(articleDto);
        article.setId(KeyGenerator.genKey());
        int result = articleDao.insert(article);
        if (result == 0) {
            log.error("[Admin-文章服务]-错误：{}", BlogErrorEnum.UNKNOWN_ERROR.getMsg());
            throw new BlogException(BlogErrorEnum.UNKNOWN_ERROR);
        }
        //文章-标签关联入库
        //获取文章标签集合
        if (!CollectionUtils.isEmpty(articleDto.getTags())) {
            List<Integer> tagIds = articleDto.getTags().stream().map(tag -> tag.getId()).collect(Collectors.toList());
            //标签表再查询一次确认确保文章标签是正确的
            List<Tag> tags = tagDao.selectByIdList(tagIds);
            List<Integer> ids = tags.stream().map(tag -> tag.getId()).collect(Collectors.toList());
            articleTagsDao.multipleInsert(article.getId(), ids);
        }
        log.info("[Admin-文章服务]-添加文章：{}", article.getId());
        return result;
    }

    //更新文章
    @Transactional
    public int update(ArticleDto articleDto) throws Exception {
        //ArticleDto => Article并更新入库
        Article article = ArticleConvertor.convertToArticle(articleDto);
        int result = articleDao.update(article);
        if (result == 0) {
            log.error("[Admin-文章服务]-错误：{}", BlogErrorEnum.UNKNOWN_ERROR.getMsg());
            throw new BlogException(BlogErrorEnum.UNKNOWN_ERROR);
        }
        //文章-标签关联更新，先清除更新前的文章-标签关联
        articleTagsDao.deleteByArticleId(article.getId());
        //先判断文章有没有新增标签，之后再插入更新后的文章-标签关联
        if (!CollectionUtils.isEmpty(articleDto.getTags())) {
            //该文章标签ID集合
            List<Integer> articleTagIdList = articleDto.getTags().stream().map(tag -> tag.getId()).collect(Collectors.toList());
            //进一步从数据库中查询存在的标签ID集合
            List<Integer> tagIdList = tagDao.selectByIdList(articleTagIdList).stream().map(tag -> tag.getId()).collect(Collectors.toList());
            articleTagsDao.multipleInsert(article.getId(), tagIdList);
        }
        log.info("[Admin-文章服务]-更新文章：{}", article.getId());
        return result;
    }

    //查询某篇文章（根据文章id）
    @Override
    public ArticleDto findById(String id) throws Exception {
        //查询文章对象
        Article article = articleDao.selectById(id);
        if (ObjectUtils.isEmpty(article)) {
            log.error("[Admin-文章服务]-错误：{}", BlogErrorEnum.ARTICLE_NO_EXIST.getMsg());
            throw new BlogException(BlogErrorEnum.ARTICLE_NO_EXIST);
        }
        //转换：Article = > ArticleDto
        ArticleDto articleDto = ArticleConvertor.convertToArticleDto(article);
        return articleDto;
    }

    //查询文章-根据文章标题
    @Override
    public PageInfo<ArticleDto> findByTitle(String title, Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize, "create_time DESC");
        List<ArticleDto> articleDotList = articleDtoDao.selectByTitle(title);
        ArticleConvertor.setTagsToArticleDto(articleDotList);
        return new PageInfo<>(articleDotList);
    }

    //查询文章集合（根据分类id）
    @Override
    public PageInfo<ArticleDto> findByCategoryId(Integer categoryId, Integer page, Integer pageSize) {
        //检验类别是否存在
        Category category = categoryDao.selectById(categoryId);
        if (ObjectUtils.isEmpty(category)) {
            log.error("[Admin-文章服务]-错误：{}", BlogErrorEnum.CATEGORY_NO_EXIST.getMsg());
            throw new BlogException(BlogErrorEnum.CATEGORY_NO_EXIST);
        }
        //分页查询获取ArticleDto集合
        PageHelper.startPage(page, pageSize, "create_time DESC");
        List<ArticleDto> articleDotList = articleDtoDao.selectByCategoryId(categoryId);
        ArticleConvertor.setTagsToArticleDto(articleDotList);
        return new PageInfo<>(articleDotList);
    }

    //查询文章集合（根据分类名称）
    @Override
    public PageInfo<ArticleDto> findByCategoryName(String categoryName, Integer page, Integer pageSize) throws Exception {
        //检验类别是否存在
        Category category = categoryDao.selectByName(categoryName);
        if (ObjectUtils.isEmpty(category)) {
            log.error("[Admin-文章服务]-错误：{}", BlogErrorEnum.CATEGORY_NO_EXIST.getMsg());
            throw new BlogException(BlogErrorEnum.CATEGORY_NO_EXIST);
        }
        //分页查询获取ArticleDto集合
        PageHelper.startPage(page, pageSize, "create_time DESC");
        List<ArticleDto> articleDotList = articleDtoDao.selectByCategoryName(categoryName);
        ArticleConvertor.setTagsToArticleDto(articleDotList);
        return new PageInfo<>(articleDotList);
    }

    //查询文章（根据标签名）
    @Override
    public PageInfo<ArticleDto> findByTagName(String tagName, Integer page, Integer pageSize) {
        //查询标签名称
        Tag tag = tagDao.selectByName(tagName);
        if (ObjectUtils.isEmpty(tag)) {
            log.error("[Admin-文章服务]-错误：{}", BlogErrorEnum.TAG_NO_EXIST.getMsg());
            throw new BlogException(BlogErrorEnum.TAG_NO_EXIST);
        }
        //分页查询获取ArticleDto集合
        PageHelper.startPage(page, pageSize, "create_time DESC");
        List<ArticleDto> articleDtoList = articleDtoDao.selectByTagName(tagName);
        ArticleConvertor.setTagsToArticleDto(articleDtoList);
        return new PageInfo<>(articleDtoList);
    }

    //查询所有文章
    @Override
    public PageInfo<ArticleDto> findAll(Integer page, Integer pageSize, Integer includeDraft) {
        //分页查询获取所有文章（ArticleDto集合）
        PageHelper.startPage(page, pageSize, "create_time DESC");
        List<ArticleDto> articleDotList = articleDtoDao.selectAll(includeDraft);
        ArticleConvertor.setTagsToArticleDto(articleDotList);
        return new PageInfo<>(articleDotList);
    }

    //文章总数
    @Override
    public int count() {
        return articleDao.count();
    }

    //统计文章总数-根据条件
    @Override
    public int countBy(String column, String value) {
        if (column.equals(ARTICLE_CATEGORY_ID)) {
            return articleDao.countByCategoryId(value);
        } else if (column.equals(ARTICLE_TITLE)) {
            return articleDao.countByTitle(value);
        } else if (column.equals(ARTICLE_TAGS_NAME)) {
            return articleTagsDao.countByTagName(value);
        }
        return 0;
    }

    //删除文章（根据文章id）
    @Override
    public int deleteById(String id) throws Exception {
        int result = articleDao.delete(id);
        if (result == 0) {
            log.error("[Admin-文章服务]-错误：{}", BlogErrorEnum.ARTICLE_NO_EXIST.getMsg());
            throw new BlogException(BlogErrorEnum.ARTICLE_NO_EXIST);
        }
        return result;
    }

    //评论数更新（发布、删除评论时）
    @Override
    public int updateCommentNum(String id, boolean isIncrease) throws Exception {
        Article article = articleDao.selectById(id);
        Integer newCommentNum = isIncrease ? article.getCommentNum() + 1 : article.getCommentNum() - 1;
        int result = articleDao.updateCommentNum(id, newCommentNum);
        return result;
    }

    //增加阅读量
    @Override
    public int incReadNum(String id) {
        return articleDao.incReadNum(id);
    }

    //点赞（增加点赞数）
    @Override
    public int like(String id) {
        return articleDao.incLikerNum(id);
    }

    //取消点赞（减少点赞数）
    @Override
    public int cancelLike(String id) {
        return articleDao.decLikerNum(id);
    }
}
