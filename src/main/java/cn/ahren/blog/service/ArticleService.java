package cn.ahren.blog.service;

import cn.ahren.blog.dto.ArticleDto;
import com.github.pagehelper.PageInfo;

import java.util.List;

//文章服务接口
public interface ArticleService {

    //数据库表列名
    String ARTICLE_CATEGORY_ID="cid";
    String ARTICLE_TITLE="title";
    String ARTICLE_TAGS_NAME="tname";   //标签表的标签字段列名

    //添加文章
    int add(ArticleDto articleDto)throws Exception;

    //更新文章
    int update(ArticleDto articleDto) throws Exception;

    //查询某篇文章（根据文章id）
    ArticleDto findById(String id) throws Exception;

    //查询文章集合（根据分类id）
    PageInfo<ArticleDto> findByCategoryId(Integer categoryId, Integer page, Integer pageSize);

    //查询文章集合（根据分类名）
    PageInfo<ArticleDto> findByCategoryName(String categoryName, Integer page, Integer pageSize) throws Exception;

    //查询文章集合（根据标签名）
    PageInfo<ArticleDto> findByTagName(String tagName,Integer page,Integer pageSize);

    //查询所有文章
    PageInfo<ArticleDto> findAll(Integer page,Integer pageSize,Integer includeDraft);

    PageInfo<ArticleDto> findByTitle(String title,Integer page,Integer pageSize);

    //文章计数（正稿）
    int count();

    //统计文章总数-根据条件（正稿）
    int countBy(String condition,String value);

    //删除文章（根据文章id）
    int deleteById(String id) throws Exception;

    //评论数更新（发布、删除评论时）
    int updateCommentNum(String id,boolean isIncrease)throws Exception;

    //增加阅读量
    int incReadNum(String id);

    //点赞（增加点赞数）
    int like(String id);

    //取消点赞（减少点赞数）
    int cancelLike(String id);
}
