package cn.ahren.blog.dto;

import cn.ahren.blog.entity.Tag;
import cn.ahren.blog.enums.ArticleStateEnum;
import lombok.Data;

import java.util.Date;
import java.util.List;

//文章DTO对象（文章+标签）
@Data
public class ArticleDto {

    private String id;              //文章id
    private String title;          //文章标题
    private String summary;        //文章简介
    private String content;        //文章内容(markdown格式)
    private String categoryName;   //<类别名称>
    private Integer state=ArticleStateEnum.FORMAL.getCode();  //文章状态：0表示正稿，1表示草稿
    private Integer commentNum;     //评论数
    private Integer likerNum;       //点赞数
    private Integer readNum;        //阅读量
    private Date createTime;      //文章创建时间
    private Date updateTime;      //文章修改时间
    private List<Tag> tags;     //<文章标签集合>
}
