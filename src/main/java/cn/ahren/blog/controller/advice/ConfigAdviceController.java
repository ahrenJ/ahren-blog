package cn.ahren.blog.controller.advice;

import cn.ahren.blog.config.BlogConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ConfigAdviceController {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @ModelAttribute
    public void putConfigValue(Model model){
        Map<String,String> configMap=new HashMap<>();
        if (redisTemplate.hasKey(BlogConfiguration.KEY)){
            configMap.put("BLOG_NAME", (String)redisTemplate.opsForHash().get(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_BLOG_NAME));
            configMap.put("OWNER_NAME", (String)redisTemplate.opsForHash().get(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_OWNER_NAME));
            configMap.put("OWNER_EMAIL", (String)redisTemplate.opsForHash().get(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_OWNER_EMAIL));
            configMap.put("GITHUB_LINK", (String)redisTemplate.opsForHash().get(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_GITHUB_LINK));
        }else{
            configMap.put("BLOG_NAME", BlogConfiguration.BLOG_NAME);
            configMap.put("OWNER_NAME", BlogConfiguration.OWNER_NAME);
            configMap.put("OWNER_EMAIL", BlogConfiguration.OWNER_EMAIL);
            configMap.put("GITHUB_LINK", BlogConfiguration.GITHUB_LINK);
        }
        model.addAllAttributes(configMap);
    }
}
