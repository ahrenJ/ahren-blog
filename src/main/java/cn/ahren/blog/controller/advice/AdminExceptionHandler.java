package cn.ahren.blog.controller.advice;

import cn.ahren.blog.config.BlogConfiguration;
import cn.ahren.blog.exception.AdminAuthorizeException;
import cn.ahren.blog.exception.AdminDuplicateLoginException;
import cn.ahren.blog.exception.IllegalAccessBlogException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

//控制层统一异常处理器
@ControllerAdvice
@Slf4j
public class AdminExceptionHandler {

    @Autowired
    private StringRedisTemplate redisTemplate;

    //处理权限异常
    @ExceptionHandler(value = AdminAuthorizeException.class)
    public ModelAndView handleAuthorizeException(){
        Map<String,String> configMap=new HashMap<>();
        if (redisTemplate.hasKey(BlogConfiguration.KEY)){
            configMap.put("BLOG_NAME", (String)redisTemplate.opsForHash().get(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_BLOG_NAME));
            configMap.put("OWNER_NAME", (String)redisTemplate.opsForHash().get(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_OWNER_NAME));
        }else{
            configMap.put("BLOG_NAME", BlogConfiguration.BLOG_NAME);
            configMap.put("OWNER_NAME", BlogConfiguration.OWNER_NAME);
        }
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.addAllObjects(configMap);
        modelAndView.setViewName("admin/login");
        return modelAndView;
    }

    //处理管理员重复登录异常
    @ExceptionHandler(value = AdminDuplicateLoginException.class)
    public ModelAndView handleDuplicateLoginException(ModelAndView modelAndView){
        Map<String,String> configMap=new HashMap<>();
        if (redisTemplate.hasKey(BlogConfiguration.KEY)){
            configMap.put("BLOG_NAME", (String)redisTemplate.opsForHash().get(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_BLOG_NAME));
            configMap.put("OWNER_NAME", (String)redisTemplate.opsForHash().get(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_OWNER_NAME));
        }else{
            configMap.put("BLOG_NAME", BlogConfiguration.BLOG_NAME);
            configMap.put("OWNER_NAME", BlogConfiguration.OWNER_NAME);
        }
        modelAndView.addAllObjects(configMap);
        modelAndView.setViewName("admin/index");
        return modelAndView;
    }

    //处理博客前台非正常访问异常
    @ExceptionHandler(value = IllegalAccessBlogException.class)
    @ResponseBody
    public String handleIllegalAccessException(){
        return "拒绝访问";
    }
}
