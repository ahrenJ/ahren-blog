package cn.ahren.blog.controller.blog;

import cn.ahren.blog.entity.Tag;
import cn.ahren.blog.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@CrossOrigin
public class BlogTagController {

    @Autowired
    private TagService tagService;

    //异步返回标签名集合
    @GetMapping("/ajax/getTags")
    @ResponseBody
    public List<Tag> getTags(){
        List<Tag> tags=tagService.findAll();
        return tags;
    }
}
