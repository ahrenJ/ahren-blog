package cn.ahren.blog.controller.blog;

import cn.ahren.blog.dto.ArticleDto;
import cn.ahren.blog.enums.ArticleStateEnum;
import cn.ahren.blog.service.ArticleService;
import cn.ahren.blog.utils.CookieUtil;
import cn.ahren.blog.vo.ArticleVo;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

//博客主站-文章列表页接口
@Controller
@Slf4j
public class BlogArticleController {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    //异步返回文章列表
    @GetMapping("/ajax/article/list")
    @ResponseBody
    public List<ArticleVo> list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                @RequestParam(value = "title", required = false) String title,
                                @RequestParam(value = "categoryId", required = false) Integer categoryId,
                                @RequestParam(value = "tagName", required = false) String tagName) {
        PageInfo<ArticleDto> articleDtoList;
        if (!StringUtils.isEmpty(title)) {
            articleDtoList = articleService.findByTitle(title, page, pageSize);
        } else if (!StringUtils.isEmpty(categoryId)) {
            articleDtoList = articleService.findByCategoryId(categoryId, page, pageSize);
        } else if (!StringUtils.isEmpty(tagName)) {
            articleDtoList = articleService.findByTagName(tagName, page, pageSize);
        } else {
            articleDtoList = articleService.findAll(page, pageSize,0);
        }
        List<ArticleVo> articleVoList = articleDtoList.getList().stream()
                .filter(articleDto -> articleDto.getState() == ArticleStateEnum.FORMAL.getCode())
                .map(articleDto -> {
                    ArticleVo articleVo = new ArticleVo();
                    BeanUtils.copyProperties(articleDto, articleVo);
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    articleVo.setCreateTime(dateFormat.format(articleDto.getCreateTime()));
                    return articleVo;
                }).collect(Collectors.toList());
        return articleVoList;
    }

    //异步返回文章总数
    @GetMapping("/ajax/article/total")
    @ResponseBody
    public int total() {
        return articleService.count();
    }

    //点赞
    @PostMapping(value = "/ajax/article/like")
    @ResponseBody
    public String like(@RequestParam("articleId") String articleId,
                       HttpServletRequest request) {
        if (StringUtils.isEmpty(articleId)) {
            return "like action error";
        }
        String cookieValue = CookieUtil.get(request, "blog_token").getValue();
        String hasLikeKey = "like-" + cookieValue;    //用于记录用户点赞过哪些文章的set集合的redis键
        log.info("articleId={}", articleId);
        if (redisTemplate.opsForSet().isMember(hasLikeKey, articleId)) {
            redisTemplate.opsForSet().remove(hasLikeKey, articleId);
            articleService.cancelLike(articleId);
            log.info("[Admin-文章服务]-取消点赞,Cookie={}", hasLikeKey);
            return "desc";
        } else {
            redisTemplate.opsForSet().add(hasLikeKey, articleId);
            redisTemplate.expire(hasLikeKey, 15, TimeUnit.DAYS);
            articleService.like(articleId);
            log.info("[Admin-文章服务]-点赞成功,Cookie={}", hasLikeKey);
            return "asc";
        }
    }
}
