package cn.ahren.blog.controller.blog;

import cn.ahren.blog.service.CategoryService;
import cn.ahren.blog.vo.CateToArticleNumVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class BlogCategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/ajax/category/list")
    @ResponseBody
    public List<CateToArticleNumVo> cateToArticleNumVoList(){
        return categoryService.findAndCountArticleNum();
    }
}
