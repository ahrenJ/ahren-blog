package cn.ahren.blog.controller.blog;

import cn.ahren.blog.entity.Reply;
import cn.ahren.blog.service.ReplyService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class BlogReplyController {

    @Autowired
    private ReplyService replyService;

    @PostMapping("/ajax/reply")
    @ResponseBody
    public String reply(@RequestBody Reply reply){
        replyService.replyToComment(reply);
        return "success";
    }

    @GetMapping("/ajax/replyList/{commentId}")
    @ResponseBody
    public List<Reply> getReplyByCommentId(@PathVariable("commentId")Integer commentId){
        PageInfo<Reply> replyPageInfo=replyService.findReplyByCommentId(commentId,1,999);
        return replyPageInfo.getList();
    }
}
