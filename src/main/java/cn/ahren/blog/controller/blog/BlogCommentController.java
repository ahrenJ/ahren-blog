package cn.ahren.blog.controller.blog;

import cn.ahren.blog.form.CommentForm;
import cn.ahren.blog.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/comment")
public class BlogCommentController {

    @Autowired
    private CommentService commentService;

    @PostMapping("/post")
    public ModelAndView post(@Valid CommentForm commentForm, BindingResult bindingResult, ModelAndView modelAndView){
        if (bindingResult.hasErrors()){
            String message=bindingResult.getFieldError().getDefaultMessage();
            modelAndView.addObject("msg",message);
        }
        try {
            commentService.comment(commentForm);
        }catch (Exception e){
            modelAndView.addObject("msg",e.getMessage());
            //返回错误提示
        }
        modelAndView.setViewName("redirect:/article/"+commentForm.getArticleId());
        return modelAndView;
    }

    //异步返回文章分类列表和对应的文章数
}
