package cn.ahren.blog.controller.blog;

import cn.ahren.blog.config.BlogConfiguration;
import cn.ahren.blog.dto.ArticleDto;
import cn.ahren.blog.entity.Comment;
import cn.ahren.blog.enums.ArticleStateEnum;
import cn.ahren.blog.form.BlogConfigForm;
import cn.ahren.blog.service.ArticleService;
import cn.ahren.blog.service.CommentService;
import cn.ahren.blog.utils.CookieUtil;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

//博客主站-页面跳转接口
@Controller
@Slf4j
public class BlogBaseController {

    @Autowired
    private ArticleService articleService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private StringRedisTemplate redisTemplate;

    //首页
    @GetMapping("")
    public ModelAndView index(ModelAndView modelAndView) {
        PageInfo<ArticleDto> pageInfo = articleService.findAll(1, 5,0);
        modelAndView.addObject("articleDtos", pageInfo.getList());
        addBlogConfigToModalView(modelAndView);
        modelAndView.setViewName("blog/index");
        return modelAndView;
    }

    //文章列表页
    @GetMapping("/article/list")
    public ModelAndView list(@RequestParam(value = "title", required = false) String title,
                             @RequestParam(value = "categoryId", required = false) Integer categoryId,
                             @RequestParam(value = "tagName", required = false) String tagName,
                             ModelAndView modelAndView) {
        Integer totalNumber = 0;
        if (!StringUtils.isEmpty(title)) {
            totalNumber = articleService.countBy(ArticleService.ARTICLE_TITLE, title);
            modelAndView.addObject("dataUrl", "/ajax/article/list?title=" + title);
        } else if (!StringUtils.isEmpty(categoryId)) {
            totalNumber = articleService.countBy(ArticleService.ARTICLE_CATEGORY_ID, categoryId.toString());
            modelAndView.addObject("dataUrl", "/ajax/article/list?categoryId=" + categoryId);
        } else if (!StringUtils.isEmpty(tagName)) {
            totalNumber = articleService.countBy(ArticleService.ARTICLE_TAGS_NAME, tagName);
            modelAndView.addObject("dataUrl", "/ajax/article/list?tagName=" + tagName);
        } else {
            totalNumber = articleService.count();
            modelAndView.addObject("dataUrl", "/ajax/article/list");
        }
        modelAndView.addObject("totalNumber", totalNumber);
        addBlogConfigToModalView(modelAndView);
        modelAndView.setViewName("blog/article_list");
        return modelAndView;
    }

    //文章详情页
    @GetMapping("/article/{articleId}")
    public ModelAndView detail(@PathVariable("articleId") String articleId,
                               HttpServletRequest request,
                               ModelAndView modelAndView) {
        ArticleDto articleDto;
        PageInfo<Comment> commentPageInfo;
        try {
            articleDto = articleService.findById(articleId);
            commentPageInfo = commentService.findByArticleId(articleId, 1, 10);
        } catch (Exception e) {
            return null;
        }
        //cookie的值，也是redis缓存中的key
        String token = CookieUtil.get(request, "blog_token").getValue();

        String hasLikeKey = "like-" + token;    //用于记录用户点赞过哪些文章的set集合的redis键
        String readNumKey = "read-" + token;    //用于记录用户看过哪些文章的set集合的redis键
        //判断是否为新浏览用户，如果是则增加文章阅读量
        if (!redisTemplate.opsForSet().isMember(readNumKey, articleId)) {
            articleService.incReadNum(articleId);
            redisTemplate.opsForSet().add(readNumKey, articleId);
            articleDto.setReadNum(articleDto.getReadNum() + 1);
        }
        //检验当前用户是否点赞
        Boolean hasLike = redisTemplate.opsForSet().isMember(hasLikeKey, articleId);
        modelAndView.addObject("hasLike", hasLike);
        modelAndView.addObject("articleDto", articleDto);
        modelAndView.addObject("commentPageInfo", commentPageInfo);
        addBlogConfigToModalView(modelAndView);
        modelAndView.setViewName("blog/detail");
        return modelAndView;
    }

    //ModalAndView添加配置类对象
    public void addBlogConfigToModalView(ModelAndView modelAndView) {
        modelAndView.addObject("blogConfig", new BlogConfiguration());
    }
}
