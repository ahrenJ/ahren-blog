package cn.ahren.blog.controller.admin;

import cn.ahren.blog.config.AdminAccount;
import cn.ahren.blog.config.BlogConfiguration;
import cn.ahren.blog.dto.ArticleDto;
import cn.ahren.blog.entity.Category;
import cn.ahren.blog.entity.Comment;
import cn.ahren.blog.entity.Reply;
import cn.ahren.blog.entity.Tag;
import cn.ahren.blog.service.*;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

//后台页面跳转控制
@Controller
@RequestMapping("/admin")
@Slf4j
public class BaseController {

    @Autowired
    private ArticleService articleService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private ReplyService replyService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private TagService tagService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    //登录页面
    @GetMapping("/login")
    public String login(){
        return "admin/login";
    }

    //首页
    @GetMapping("")
    public String index(){
        return "admin/index";
    }

    //文章列表页
    @GetMapping("/article/list")
    public ModelAndView list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                             @RequestParam(value = "pageSize",defaultValue = "10")Integer pageSize,
                             ModelAndView modelAndView){

        PageInfo<ArticleDto> articleDtoPageInfo=articleService.findAll(page,pageSize,1);

        modelAndView.addObject("articleDtoPageInfo",articleDtoPageInfo);
        modelAndView.addObject("currentPage",articleDtoPageInfo.getPageNum());
        modelAndView.addObject("size",articleDtoPageInfo.getPageSize());
        modelAndView.setViewName("admin/article_list");

        return modelAndView;
    }

    //文章发布页
    @GetMapping("/article/post")
    public ModelAndView articlePost(ModelAndView modelAndView){
        List<Category> categories=categoryService.findAll();
        List<Tag> tags=tagService.findAll();

        modelAndView.addObject("categories",categories);
        modelAndView.addObject("tags",tags);
        modelAndView.setViewName("admin/article_post");

        return modelAndView;
    }

    //文章编辑页
    @GetMapping("/article/{articleId}")
    public ModelAndView articleEdit(@PathVariable("articleId")String articleId, ModelAndView modelAndView){
        ArticleDto articleDto;
        try{
            articleDto=articleService.findById(articleId);
        }catch (Exception e){
            modelAndView.addObject("msg",e.getMessage());
            modelAndView.setViewName("admin/common/error");
            return modelAndView;
        }
        List<Category> categories=categoryService.findAll();
        List<Tag> tags=tagService.findAll();

        modelAndView.addObject("categories",categories);
        modelAndView.addObject("tags",tags);
        modelAndView.addObject("articleDto",articleDto);
        modelAndView.setViewName("admin/article_edit");

        return modelAndView;
    }

    //评论详情页
    @RequestMapping("/comment/{commentId}")
    public ModelAndView commentDetail(@PathVariable("commentId")Integer commentId,ModelAndView modelAndView){
        Comment comment;
        try {
            comment=commentService.findById(commentId);
        }catch (Exception e){
            modelAndView.addObject("msg",e.getMessage());
            modelAndView.setViewName("admin/common/error");
            return modelAndView;
        }
        //获取该评论的所有回复
        PageInfo<Reply> replyPageInfo=replyService.findReplyByCommentId(commentId,1,9999);

        modelAndView.addObject("comment",comment);
        modelAndView.addObject("replies",replyPageInfo.getList());
        modelAndView.setViewName("admin/comment_detail");

        return modelAndView;
    }

    //分类列表页
    @GetMapping("/category/list")
    public ModelAndView categoryList(@RequestParam(value = "page",defaultValue = "1")Integer page,
                                           @RequestParam(value = "pageSize",defaultValue = "10")Integer pageSize,
                                           ModelAndView modelAndView){
        PageInfo<Category> categoryPageInfo=categoryService.findAll(page,pageSize);
        modelAndView.addObject("categoryPageInfo",categoryPageInfo);
        modelAndView.addObject("currentPage",categoryPageInfo.getPageNum());
        modelAndView.addObject("size",categoryPageInfo.getPageSize());
        modelAndView.setViewName("admin/category_list");
        return modelAndView;
    }

    //标签列表页
    @GetMapping("/tag/list")
    public ModelAndView tagList(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                ModelAndView modelAndView) {
        PageInfo<Tag> tagPageInfo = tagService.findAll(page,pageSize);
        modelAndView.addObject("tagPageInfo", tagPageInfo);
        modelAndView.addObject("currentPage", tagPageInfo.getPageNum());
        modelAndView.addObject("size", tagPageInfo.getPageSize());
        modelAndView.setViewName("admin/tag_list");
        return modelAndView;
    }

    //系统设置页
    @GetMapping("/config")
    public ModelAndView config(ModelAndView modelAndView){
        //获取账户名
        if (redisTemplate.hasKey(AdminAccount.KEY)){
            String accountName= (String) redisTemplate.opsForHash().get(AdminAccount.KEY,AdminAccount.HASH_KEY_ACCOUNT_NAME);
            modelAndView.addObject("accountName",accountName);
        }else{
            modelAndView.addObject("accountName",AdminAccount.ACCOUNT_NAME);
        }
        //获取网站配置信息
        if (redisTemplate.hasKey(BlogConfiguration.KEY)){
            modelAndView.addObject("blogName",redisTemplate.opsForHash().get(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_BLOG_NAME));
            modelAndView.addObject("ownerName",redisTemplate.opsForHash().get(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_OWNER_NAME));
            modelAndView.addObject("ownerEmail",redisTemplate.opsForHash().get(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_OWNER_EMAIL));
            modelAndView.addObject("githubLink",redisTemplate.opsForHash().get(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_GITHUB_LINK));
            modelAndView.addObject("openOutside",Integer.valueOf((String)redisTemplate.opsForHash().get(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_OPEN_OUTSIDE)));
        }else {
            modelAndView.addObject("blogName",BlogConfiguration.BLOG_NAME);
            modelAndView.addObject("ownerName",BlogConfiguration.OWNER_NAME);
            modelAndView.addObject("ownerEmail",BlogConfiguration.OWNER_EMAIL);
            modelAndView.addObject("githubLink",BlogConfiguration.GITHUB_LINK);
            modelAndView.addObject("openOutside",BlogConfiguration.OPEN_OUTSIDE);
        }
        modelAndView.setViewName("admin/config");
        return modelAndView;
    }
}
