package cn.ahren.blog.controller.admin;

import cn.ahren.blog.dto.ArticleDto;
import cn.ahren.blog.entity.Tag;
import cn.ahren.blog.form.ArticleForm;
import cn.ahren.blog.service.ArticleService;
import cn.ahren.blog.service.TagService;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

//后台文章操作接口
@Controller
@RequestMapping("/admin")
@Slf4j
public class ArticleController {

    @Autowired
    private ArticleService articleService;
    @Autowired
    private TagService tagService;

    //添加文章
    @PostMapping("/article/add")
    public ModelAndView add(@Valid ArticleForm articleForm,
                            BindingResult bindingResult,
                            ModelAndView modelAndView){
        modelAndView.addObject("returnUrl","article/list");
        //表单校验
        if (bindingResult.hasErrors()){
            String msg=bindingResult.getFieldError().getDefaultMessage();
            modelAndView.addObject("msg",msg);
            modelAndView.setViewName("admin/common/error");
            return modelAndView;
        }
        try {
            ArticleDto articleDto=new ArticleDto();
            BeanUtils.copyProperties(articleForm,articleDto);
            if (!CollectionUtils.isEmpty(articleForm.getTagIds())){
                List<Tag> tags=tagService.findByIdList(articleForm.getTagIds());
                articleDto.setTags(tags);
            }
            articleService.add(articleDto);
        }catch (Exception e){
            modelAndView.addObject("msg",e.getMessage());
            modelAndView.setViewName("admin/common/error");
            return modelAndView;
        }
        modelAndView.setViewName("admin/common/success");
        return modelAndView;
    }

    //更新文章
    @PostMapping("/article/save")
    public ModelAndView save(@Valid ArticleForm articleForm,
                             BindingResult bindingResult,
                             ModelAndView modelAndView){
        modelAndView.addObject("returnUrl","article/list");
        if (bindingResult.hasErrors()){
            String msg=bindingResult.getFieldError().getDefaultMessage();
            modelAndView.addObject("msg",msg);
            modelAndView.setViewName("admin/common/error");
            return modelAndView;
        }
        try {
            ArticleDto articleDto=new ArticleDto();
            BeanUtils.copyProperties(articleForm,articleDto);
            if (!CollectionUtils.isEmpty(articleForm.getTagIds())){
                List<Tag> tags=tagService.findByIdList(articleForm.getTagIds());
                articleDto.setTags(tags);
            }
            articleService.update(articleDto);
        }catch (Exception e){
            modelAndView.addObject("msg",e.getMessage());
            modelAndView.setViewName("admin/common/error");
            return modelAndView;
        }
        modelAndView.setViewName("admin/common/success");
        return modelAndView;
    }

    //删除文章
    @RequestMapping("/article/{articleId}/del")
    public ModelAndView delete(@PathVariable("articleId")String articleId,ModelAndView modelAndView){

        modelAndView.addObject("returnUrl","article/list");
        try {
            articleService.deleteById(articleId);
        }catch (Exception e){
            modelAndView.addObject("msg",e.getMessage());
            modelAndView.setViewName("admin/common/error");
            return modelAndView;
        }
        modelAndView.setViewName("admin/common/success");
        return modelAndView;
    }
}
