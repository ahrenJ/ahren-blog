package cn.ahren.blog.controller.admin;

import cn.ahren.blog.entity.Tag;
import cn.ahren.blog.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin/tag")
public class TagController {

    @Autowired
    private TagService tagService;

    //添加标签
    @PostMapping("/add")
    public ModelAndView add(@RequestParam("name")String name,ModelAndView modelAndView){
        Tag tag=new Tag();
        tag.setName(name);
        tagService.add(tag);
        modelAndView.setViewName("redirect:/admin/tag/list");
        return modelAndView;
    }

    //更新标签
    @PostMapping("/update")
    public ModelAndView update(@RequestParam("tagId")Integer tagId,
                               @RequestParam("tagName")String tagName,
                               ModelAndView modelAndView){
        Tag tag=new Tag(tagId,tagName);
        tagService.update(tag);
        modelAndView.setViewName("redirect:/admin/tag/list");
        return modelAndView;
    }

    //删除标签
    @GetMapping("/{id}/delete")
    public ModelAndView delete(@PathVariable("id")Integer id,ModelAndView modelAndView){
        tagService.delete(id);
        modelAndView.setViewName("redirect:/admin/tag/list");
        return modelAndView;
    }
}
