package cn.ahren.blog.controller.admin;

import cn.ahren.blog.config.AdminAccount;
import cn.ahren.blog.config.BlogConfiguration;
import cn.ahren.blog.exception.BlogException;
import cn.ahren.blog.form.AlterConfigJSON;
import cn.ahren.blog.form.BlogConfigForm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping("/admin")
@Slf4j
public class ConfigController {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @PostMapping("/alter/account")
    @ResponseBody
    public String alterAccount(@RequestBody AlterConfigJSON alterConfigJSON){
        //新账户名和新密码
        String newAccountName=alterConfigJSON.getAccountName();
        String newPassword=alterConfigJSON.getPassword();
        if (StringUtils.isEmpty(newAccountName))
            return "alter failure";
        if (!newPassword.equals(alterConfigJSON.getConfirmPassword()))
            return "alter failure";

        AdminAccount.ACCOUNT_NAME=newAccountName;
        AdminAccount.ACCOUNT_PASSWORD=newPassword;
        redisTemplate.opsForHash().put(AdminAccount.KEY,AdminAccount.HASH_KEY_ACCOUNT_NAME,newAccountName);
        redisTemplate.opsForHash().put(AdminAccount.KEY,AdminAccount.HASH_KEY_ACCOUNT_PASSWORD,newPassword);
        redisTemplate.expire(AdminAccount.KEY,99999,TimeUnit.DAYS);
        log.info("[Admin-管理员账户修改]：修改成功");
        return "success";
    }

    @PostMapping("/alter/blogConfig")
    public ModelAndView alterBlogConfig(@Valid BlogConfigForm configForm,
                                        BindingResult bindingResult,
                                        ModelAndView modelAndView){
        if (bindingResult.hasErrors()){
            log.error("[Admin-网站设置]：参数错误");
            throw new BlogException(bindingResult.getFieldError().getDefaultMessage());
        }
        BlogConfiguration.BLOG_NAME=configForm.getBlogName();
        BlogConfiguration.OWNER_NAME=configForm.getOwnerName();
        BlogConfiguration.OWNER_EMAIL=configForm.getOwnerEmail();
        BlogConfiguration.GITHUB_LINK=configForm.getGithubLink();
        BlogConfiguration.OPEN_OUTSIDE=configForm.getOpenOutside();
        //存储到Redis缓存中
        redisTemplate.opsForHash().put(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_BLOG_NAME,configForm.getBlogName());
        redisTemplate.opsForHash().put(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_OWNER_NAME,configForm.getOwnerName());
        redisTemplate.opsForHash().put(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_OWNER_EMAIL,configForm.getOwnerEmail());
        redisTemplate.opsForHash().put(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_GITHUB_LINK,configForm.getGithubLink());
        redisTemplate.opsForHash().put(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_OPEN_OUTSIDE,configForm.getOpenOutside().toString());
        redisTemplate.expire(BlogConfiguration.KEY,999999,TimeUnit.DAYS);
        log.info("[Admin-网站设置]：网站设置修改成功");
        modelAndView.setViewName("redirect:/admin/config");
        return modelAndView;
    }
}
