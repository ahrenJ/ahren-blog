package cn.ahren.blog.controller.admin;

import cn.ahren.blog.entity.Category;
import cn.ahren.blog.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    //添加分类
    @PostMapping("/add")
    public ModelAndView add(@RequestParam("name")String name, ModelAndView modelAndView){
        Category category=new Category();
        category.setName(name);
        categoryService.add(category);
        modelAndView.setViewName("redirect:/admin/category/list");
        return modelAndView;
    }

    //修改分类
    @PostMapping("/update")
    public ModelAndView update(@RequestParam("categoryId")Integer categoryId,
                               @RequestParam("categoryName")String categoryName,
                               ModelAndView modelAndView){
        Category category=new Category(categoryId,categoryName);
        categoryService.update(category);
        modelAndView.setViewName("redirect:/admin/category/list");
        return modelAndView;
    }

    //删除分类
    @GetMapping("/{id}/delete")
    public ModelAndView update(@PathVariable("id")Integer id, ModelAndView modelAndView){
        categoryService.delete(id);
        modelAndView.setViewName("redirect:/admin/category/list");
        return modelAndView;
    }
}
