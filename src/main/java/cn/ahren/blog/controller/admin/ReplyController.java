package cn.ahren.blog.controller.admin;

import cn.ahren.blog.service.ReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin")
public class ReplyController {

    @Autowired
    private ReplyService replyService;

    //删除回复
    @GetMapping("/reply/{id}/del")
    public ModelAndView delete(@PathVariable("id")Integer id,
                               @RequestParam("commentId")Integer commentId,
                               ModelAndView modelAndView){

        modelAndView.addObject("returnUrl","comment/"+commentId);
        try {
            replyService.delete(id,commentId);
        }catch (Exception e){
            modelAndView.addObject("msg",e.getMessage());
            modelAndView.setViewName("admin/common/error");
            return modelAndView;
        }
        modelAndView.setViewName("admin/common/success");
        return modelAndView;
    }
}
