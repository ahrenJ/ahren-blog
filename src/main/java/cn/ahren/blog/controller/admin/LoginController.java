package cn.ahren.blog.controller.admin;

import cn.ahren.blog.config.AdminAccount;
import cn.ahren.blog.form.LoggingJSON;
import cn.ahren.blog.utils.CookieUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

//后台管理员登录接口
@Controller
@RequestMapping("/admin")
@Slf4j
public class LoginController {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @PostMapping(value = "/logining")
    @ResponseBody
    public String logging(@RequestBody LoggingJSON loggingForm, HttpServletResponse response) {
        //先从Redis缓存中判断是否有保存已修改后的管理员账户
        if (redisTemplate.hasKey(AdminAccount.KEY)){
            String accountName=(String)redisTemplate.opsForHash().get(AdminAccount.KEY,AdminAccount.HASH_KEY_ACCOUNT_NAME);
            String password=(String)redisTemplate.opsForHash().get(AdminAccount.KEY,AdminAccount.HASH_KEY_ACCOUNT_PASSWORD);
            if (accountName.equals(loggingForm.getAccountName())&&
                    password.equals(loggingForm.getPassword())){
                //生成Cookie并返回给浏览器
                String token = UUID.randomUUID().toString();
                CookieUtil.set("admin_token",response, token, 3);
                log.info("[Admin-后台管理员登录(Redis)]：登录成功,Cookie={}",token);
                return "success";
            }else{
                //与Redis缓存的账户不一致
                return "logging failure";
            }
        }
        //Redis缓存中无管理员账户，说明管理员账户未被修改或Redis已宕机
        if (AdminAccount.ACCOUNT_NAME.equals(loggingForm.getAccountName()) &&
                AdminAccount.ACCOUNT_PASSWORD.equals(loggingForm.getPassword())) {
            //生成Cookie并返回给浏览器
            String token = UUID.randomUUID().toString();
            CookieUtil.set("admin_token",response, token, 3);
            log.info("[Admin-后台管理员登录]：登录成功,Cookie={}",token);
            return "success";
        }else{
            log.info("[Admin-后台管理员登录]：登录失败");
            return "logging failure";
        }
    }
}
