package cn.ahren.blog.controller.admin;

import cn.ahren.blog.entity.Comment;
import cn.ahren.blog.entity.Reply;
import cn.ahren.blog.service.CommentService;
import cn.ahren.blog.service.ReplyService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

//后台评论操作接口
@Controller
@RequestMapping("/admin")
public class CommentController {

    @Autowired
    private CommentService commentService;
    @Autowired
    private ReplyService replyService;

    //评论列表
    @GetMapping("/comment/list")
    public ModelAndView list(@RequestParam(value = "page",defaultValue = "1")Integer page,
                             @RequestParam(value = "pageSize",defaultValue = "8")Integer pageSize,
                             ModelAndView modelAndView){
        PageInfo<Comment> pageInfo=commentService.findAll(page,pageSize);
        modelAndView.addObject("commentPageInfo",pageInfo);
        modelAndView.addObject("currentPage",page);
        modelAndView.addObject("size",pageSize);
        modelAndView.setViewName("admin/comment_list");
        return modelAndView;
    }

    //评论详情
    @GetMapping("/comment/{commentId}")
    public ModelAndView detail(@PathVariable("commentId")Integer commentId,
                               ModelAndView modelAndView){

        Comment comment;
        PageInfo<Reply> repliesPageInfo;

        try {
            comment=commentService.findById(commentId);
            repliesPageInfo=replyService.findReplyByCommentId(commentId,1,9999);
        }catch (Exception e){
            modelAndView.addObject("msg",e.getMessage());
            modelAndView.setViewName("/admin/common/error");
            return modelAndView;
        }
        modelAndView.addObject("comment",comment);
        modelAndView.addObject("replies",repliesPageInfo.getList());
        modelAndView.setViewName("/admin/comment_detail");
        return modelAndView;
    }

    //删除评论
    @GetMapping("/comment/{id}/del")
    public ModelAndView delete(@PathVariable("id")Integer id,
                               @RequestParam("articleId")String articleId ,
                               ModelAndView modelAndView){

        modelAndView.addObject("returnUrl","comment/list");
        try {
            commentService.delete(id,articleId);
        }catch (Exception e){
            modelAndView.addObject("msg",e.getMessage());
            modelAndView.setViewName("/admin/common/error");
            return modelAndView;
        }
        modelAndView.setViewName("/admin/common/success");
        return modelAndView;
    }
}
