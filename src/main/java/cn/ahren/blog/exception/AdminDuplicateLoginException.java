package cn.ahren.blog.exception;

//管理员重复登录异常
public class AdminDuplicateLoginException extends RuntimeException{

    public AdminDuplicateLoginException() {
    }

    public AdminDuplicateLoginException(String message) {
        super(message);
    }
}
