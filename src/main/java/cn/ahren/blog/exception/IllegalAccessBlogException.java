package cn.ahren.blog.exception;

public class IllegalAccessBlogException extends RuntimeException{

    public IllegalAccessBlogException() {
    }

    public IllegalAccessBlogException(String message) {
        super(message);
    }
}
