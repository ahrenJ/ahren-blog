package cn.ahren.blog.exception;

import cn.ahren.blog.enums.BlogErrorEnum;

public class BlogException extends RuntimeException{

    private String msg;

    public BlogException(BlogErrorEnum errorEnum){
        super(errorEnum.getMsg());
    }

    public BlogException(String message) {
        super(message);
    }
}
