package cn.ahren.blog.exception;

//后台管理系统权限验证异常
public class AdminAuthorizeException extends RuntimeException{

    public AdminAuthorizeException() {
    }

    public AdminAuthorizeException(String message) {
        super(message);
    }
}
