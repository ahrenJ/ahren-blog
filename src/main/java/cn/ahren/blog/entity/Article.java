package cn.ahren.blog.entity;

import cn.ahren.blog.enums.ArticleStateEnum;
import lombok.Data;

import java.util.Date;

//文章实体类
@Data
public class Article {

    private String id;          //文章id
    private String title;       //文章标题
    private String summary;     //文章简介
    private String content;     //文章内容(markdown格式)
    private Integer categoryId; //类别id
    //文章状态：0表示正稿，1表示草稿
    private Integer state=ArticleStateEnum.FORMAL.getCode();
    private Integer commentNum; //评论数
    private Integer likerNum;   //点赞数
    private Integer readNum;    //阅读量
    private Date createTime;    //文章创建时间
    private Date updateTime;    //文章修改时间

}
