package cn.ahren.blog.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

//文章标签实体类
@Data
public class Tag {

    public Tag() {
    }

    public Tag(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    private Integer id;     //标签Id
    private String name;    //标签名称
}
