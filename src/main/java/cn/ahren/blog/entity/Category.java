package cn.ahren.blog.entity;

import lombok.Data;

//文章类别实体类
@Data
public class Category {

    private Integer id;     //类别id
    private String name;    //类别名称

    public Category() {
    }

    public Category(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
}
