package cn.ahren.blog.entity;

import lombok.Data;

//文章-标签关联实体类
@Data
public class ArticleTags {

    private Integer id;          //id
    private String articleId;   //文章id
    private Integer tagId;       //标签id
}
