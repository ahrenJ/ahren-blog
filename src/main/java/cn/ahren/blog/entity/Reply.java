package cn.ahren.blog.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

//回复实体类（文章评论的回复）
@Data
public class Reply {

    private Integer id;          //回复者id
    private Integer commentId;  //评论id
    private Integer toId=-1;     //被回复者id，-1则表示无
    private String userName;    //回复者用户名称
    private String userEmail;   //回复者用户邮箱
    private String content;     //回复内容
    private Integer likerNum;   //点赞数
    private Integer replyNum;   //回复数
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm",timezone = "GMT+8")
    private Date createTime;    //回复时间
}
