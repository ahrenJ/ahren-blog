package cn.ahren.blog.entity;

import lombok.Data;

import java.util.Date;

//文章评论表
@Data
public class Comment {

    private Integer id;         //评论id
    private String articleId;   //文章id
    private String userName;    //评论用户名称
    private String userEmail;   //评论用户邮箱
    private String content;     //评论内容
    private Integer likerNum;   //点赞数
    private Integer replyNum;   //回复数
    private Date createTime;    //评论时间
}
