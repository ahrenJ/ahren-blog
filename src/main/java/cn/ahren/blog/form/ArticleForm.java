package cn.ahren.blog.form;

import cn.ahren.blog.enums.ArticleStateEnum;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

//文章表单对象
@Data
public class ArticleForm {

    private String id;

    @NotBlank(message = "标题不能为空")
    private String title;

    private String summary;

    private List<Integer> tagIds;

    @NotBlank(message = "文章类别不能为空")
    private String categoryName;

    @NotBlank(message = "文章内容不能为空")
    private String content;
    
    private Integer state=ArticleStateEnum.FORMAL.getCode();

}
