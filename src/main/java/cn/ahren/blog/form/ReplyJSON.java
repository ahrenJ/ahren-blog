package cn.ahren.blog.form;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ReplyJSON {

    @NotNull(message = "评论ID不能为空")
    private Integer commentId;  //评论id

    private Integer toId=-1;     //被回复者id，-1则表示无

    @NotNull(message = "用户名不能为空")
    private String userName;    //回复者用户名称

    private String userEmail;   //回复者用户邮箱

    @NotNull(message = "回复内容不能为空")
    private String content;     //回复内容
}
