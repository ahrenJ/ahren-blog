package cn.ahren.blog.form;

import lombok.Data;

@Data
public class AlterConfigJSON {

    private String accountName;
    private String password;
    private String confirmPassword;
}
