package cn.ahren.blog.form;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

//评论表单对象
@Data
public class CommentForm {

    @NotNull(message = "文章ID不能为空")
    private String articleId;   //文章id

    @NotEmpty(message = "用户名不能为空")
    private String userName;    //评论用户名称

    private String userEmail;   //评论用户邮箱

    @NotEmpty(message = "评论内容不能为空")
    private String content;     //评论内容
}
