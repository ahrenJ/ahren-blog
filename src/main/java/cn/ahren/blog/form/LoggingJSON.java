package cn.ahren.blog.form;

import lombok.Data;

@Data
public class LoggingJSON {
    private String accountName;
    private String password;
}
