package cn.ahren.blog.form;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class BlogConfigForm {

    @NotEmpty(message = "博客名称不能为空")
    public String blogName;  //博客名称

    public String ownerName;        //博主名称
    public String ownerEmail;   //博主邮箱
    public String githubLink;   //博主Github链接

    @NotNull(message = "开放选择不能为空")
    public Integer openOutside;   //博客前台是否对外开放：1-开放 0-关闭
}
