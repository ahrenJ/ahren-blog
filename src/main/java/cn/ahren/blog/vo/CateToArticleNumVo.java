package cn.ahren.blog.vo;

import lombok.Data;

//视图对象-分类与其对应的文章数量
@Data
public class CateToArticleNumVo {

    private Integer id;
    private String name;
    private String articleNum;
}
