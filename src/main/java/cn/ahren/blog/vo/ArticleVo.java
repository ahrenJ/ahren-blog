package cn.ahren.blog.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

//文章列表页视图对象
@Data
public class ArticleVo {

    private String id;
    private String title;
    private String categoryName;
    private String createTime;

}
