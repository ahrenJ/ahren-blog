package cn.ahren.blog.enums;

import lombok.Getter;

@Getter
public enum BlogErrorEnum {

    ARTICLE_NO_EXIST(1001,"文章不存在"),
    CATEGORY_NO_EXIST(1002,"分类不存在"),
    TAG_NO_EXIST(1003,"标签不存在"),
    COMMENT_NO_EXIST(1004,"评论不存在"),
    REPLY_NO_EXIST(1005,"回复不存在"),

    ARTICLE_POST_ERROR(201,"发布文章失败"),
    CATEGORY_ADD_ERROR(202,"添加分类失败"),
    TAG_ADD_ERROR(203,"添加标签失败"),
    COMMENT_POST_ERROR(204,"发表评论失败"),
    REPLY_POST_ERROR(205,"发表回复失败"),

    ARTICLE_UPDATE_ERROR(201,"更新文章失败"),
    CATEGORY_UPDATE_ERROR(202,"更新分类失败"),
    TAG_UPDATE_ERROR(203,"更新标签失败"),

    UNKNOWN_ERROR(200,"未知错误"),
    ;

    private Integer code;
    private String msg;

    BlogErrorEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
