package cn.ahren.blog.enums;

import lombok.Getter;

//文章编辑状态枚举类(正文/草稿)
@Getter
public enum ArticleStateEnum {
    FORMAL(0,"正稿"),
    DRAFT(1,"草稿")
    ;

    private Integer code;
    private String msg;

    ArticleStateEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
