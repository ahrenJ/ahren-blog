package cn.ahren.blog.config;

//博客系统设置
public class BlogConfiguration {

    //博客名称
    public static String BLOG_NAME="ahren's Blog";
    //博主名称
    public static String OWNER_NAME="ahren";
    //博主邮箱
    public static String OWNER_EMAIL="huang_zhaorui@163.com";
    //博主Github链接
    public static String GITHUB_LINK="https://github.com/ahrenJ";
    //博客前台是否对外开放：1-开放 0-关闭
    public static Integer OPEN_OUTSIDE=1;

    //Redis缓存中hash结构的Key和HashKey
    public static String KEY="blogConfig";
    public static String HASH_KEY_BLOG_NAME="blogName";
    public static String HASH_KEY_OWNER_NAME="ownerName";
    public static String HASH_KEY_OWNER_EMAIL="ownerEmail";
    public static String HASH_KEY_GITHUB_LINK="githubLink";
    public static String HASH_KEY_OPEN_OUTSIDE="openOutside";
}
