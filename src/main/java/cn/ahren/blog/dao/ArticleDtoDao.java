package cn.ahren.blog.dao;

import cn.ahren.blog.dto.ArticleDto;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

import java.util.List;

//文章表与类别表关联查询 => ArticleDto
@Mapper
@Repository
public interface ArticleDtoDao {

    //根据id查找文章
    @Select("SELECT article.id,title,summary,content,cname,state," +
            "comment_num,liker_num,read_num,create_time,update_time FROM article " +
            "INNER JOIN category ON article.cid=category.id " +
            "AND article.id=#{id}")
    @ResultMap("articleDto")
    ArticleDto selectById(@Param("id") String id);

    //查找文章（根据标签名）
    @Select("SELECT article.id,title,summary,content,cname,state," +
            "comment_num,liker_num,read_num,create_time,update_time FROM article " +
            "INNER JOIN category ON article.cid=category.id "+
            "AND article.id IN (SELECT article_tags.aid FROM article_tags " +
            "INNER JOIN tags ON article_tags.tid=tags.id AND tags.tname=#{name})")
    @ResultMap("articleDto")
    List<ArticleDto> selectByTagName(@Param("name") String name);

    //查找文章（根据标题)
    @Select("SELECT article.id,title,summary,content,cname,state," +
            "comment_num,liker_num,read_num,create_time,update_time FROM article " +
            "INNER JOIN category ON article.cid=category.id "+
            "WHERE article.title LIKE CONCAT('%',#{title},'%') AND article.state=0")
    @ResultMap("articleDto")
    List<ArticleDto> selectByTitle(@Param("title") String title);

    //查找文章（根据分类id）
    @Select("SELECT article.id,title,summary,content,cname,state,comment_num," +
            "liker_num,read_num,create_time,update_time FROM article INNER JOIN " +
            "category ON article.cid=category.id AND category.id=#{categoryId} AND article.state=0")
    @ResultMap("articleDto")
    List<ArticleDto> selectByCategoryId(@Param("categoryId")Integer categoryId);

    //查找文章（根据分类名称）
    @Select("SELECT article.id,title,summary,content,cname,state,comment_num," +
            "liker_num,read_num,create_time,update_time FROM article INNER JOIN " +
            "category ON article.cid=category.id AND category.cname=#{name} AND article.state=0")
    @ResultMap("articleDto")
    List<ArticleDto> selectByCategoryName(@Param("name") String name);

    //查找所有文章
    @Select("<script>" +
            "SELECT article.id,title,summary,content,cname,state,comment_num," +
            "liker_num,read_num,create_time,update_time FROM article INNER JOIN " +
            "category ON article.cid=category.id" +
            "<if test='includeDraft!=1'>" +
            "AND article.state=0" +
            "</if>" +
            "</script>")
    @Results(id = "articleDto",value = {@Result(property = "id",column = "id",jdbcType = JdbcType.VARCHAR),
            @Result(property = "title",column = "title",jdbcType = JdbcType.VARCHAR),
            @Result(property = "summary",column = "summary",jdbcType = JdbcType.VARCHAR),
            @Result(property = "content",column = "content",jdbcType = JdbcType.VARCHAR),
            @Result(property = "categoryName",column = "cname",jdbcType = JdbcType.VARCHAR),
            @Result(property = "state",column = "state",jdbcType = JdbcType.INTEGER),
            @Result(property = "commentNum",column = "comment_num",jdbcType = JdbcType.INTEGER),
            @Result(property = "likerNum",column = "liker_num",jdbcType = JdbcType.INTEGER),
            @Result(property = "readNum",column = "read_num",jdbcType = JdbcType.INTEGER),
            @Result(property = "createTime",column = "create_time",jdbcType = JdbcType.TIMESTAMP),
            @Result(property = "updateTime",column = "update_time",jdbcType = JdbcType.TIMESTAMP)})
    List<ArticleDto> selectAll(@Param("includeDraft") Integer includeDraft);
}
