package cn.ahren.blog.dao;

import cn.ahren.blog.entity.ArticleTags;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ArticleTagsDao {

    //添加文章-标签关联
    @Insert("INSERT INTO article_tags(aid,tid) VALUES(#{articleId},#{tagId})")
    int insert(ArticleTags articleTags);

    //批量添加文章-标签关联
    @Insert("<script>INSERT INTO article_tags(aid,tid) VALUES" +
                "<foreach item='tid' collection='tagIdList' separator=','>" +
                    "(#{articleId},#{tid})" +
                "</foreach>" +
            "</script>")
    int multipleInsert(@Param("articleId") String articleId,@Param("tagIdList") List<Integer> tagIdList);

    //删除文章-标签关联
    @Delete("DELETE FROM article_tags WHERE id=#{id}")
    int delete(Integer id);

    //根据文章id删除文章-标签关联
    @Delete("DELETE FROM article_tags WHERE aid=#{articleId}")
    int deleteByArticleId(String articleId);

    //根据标签id查询关联
    @Select("SELECT * FROM article_tags WHERE tid=#{tagId}")
    @ResultMap("articleTags")
    List<ArticleTags> selectByTagId(Integer tagId);

    //根据文章id查询关联
    @Select("SELECT * FROM article_tags WHERE aid=#{articleId}")
    @Results(id = "articleTags",value = {
            @Result(property = "id",column = "id",jdbcType = JdbcType.INTEGER),
            @Result(property = "articleId",column = "aid",jdbcType = JdbcType.VARCHAR),
            @Result(property = "tagId",column = "tid",jdbcType = JdbcType.INTEGER)})
    List<ArticleTags> selectByArticleId(String articleId);

    //根据标签名称统计文章数量（正稿）
    @Select("SELECT COUNT(*) FROM article_tags " +
            "INNER JOIN tags ON article_tags.tid=tags.id AND tags.tname=#{tagName} " +
            "INNER JOIN article ON article_tags.aid=article.id AND article.state=0 ")
    int countByTagName(@Param("tagName")String tagName);
}
