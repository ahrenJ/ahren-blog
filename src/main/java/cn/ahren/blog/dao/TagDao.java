package cn.ahren.blog.dao;

import cn.ahren.blog.entity.Tag;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TagDao {

    //添加标签
    @Insert("INSERT INTO tags(tname) VALUES(#{name})")
    int insert(Tag tag);

    //修改标签
    @Update("UPDATE tags SET tname=#{name} WHERE id=#{id}")
    int update(Tag tag);

    //删除标签
    @Delete("DELETE FROM tags WHERE id=#{id}")
    int delete(@Param("id")Integer id);

    //查找标签（根据id）
    @ResultMap("tag")
    @Select("SELECT * FROM tags WHERE id=#{id}")
    Tag selectById(@Param("id") Integer id);

    //查找标签(根据标签名称)
    @ResultMap("tag")
    @Select("SELECT * FROM tags WHERE tname=#{name}")
    Tag selectByName(@Param("name") String name);

    //查找标签(根据标签名称集合)
    @ResultMap("tag")
    @Select("<script>SELECT * FROM tags WHERE tname IN " +
                "<foreach item='name' collection='nameList' open='(' separator=',' close=')'>" +
                    "#{name}" +
                "</foreach>" +
            "</script>")
    List<Tag> selectByNameList(@Param("nameList") List<String> nameList);

    //查询标签（根据标签id集合）
    @ResultMap("tag")
    @Select("<script>SELECT * FROM tags WHERE id IN " +
                "<foreach item='tid' collection='idList' open='(' separator=',' close=')'>" +
                    "#{tid}" +
                "</foreach>" +
            "</script>")
    List<Tag> selectByIdList(@Param("idList") List<Integer> idList);

    //查询所有标签
    @Select("SELECT * FROM tags")
    @Results(id = "tag",value = {
            @Result(property = "id",column = "id",jdbcType = JdbcType.INTEGER),
            @Result(property = "name",column = "tname",jdbcType = JdbcType.VARCHAR)})
    List<Tag> selectAll();
}
