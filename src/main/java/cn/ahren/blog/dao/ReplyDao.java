package cn.ahren.blog.dao;

import cn.ahren.blog.entity.Reply;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface ReplyDao {

    //添加子评论
    @Insert("INSERT INTO replys(id,cid,to_id,cname,email,content) " +
            "VALUES(#{id},#{commentId},#{toId},#{userName},#{userEmail},#{content})")
    int insert(Reply reply);

    //删除子评论
    @Delete("DELETE FROM replys WHERE id=#{id}")
    int delete(@Param("id") Integer id);

    //删除某条评论的所有回复
    @Delete("DELETE FROM replys WHERE cid=#{commentId}")
    int deleteByCommentId(@Param("commentId")Integer commentId);

    //根据评论id查找子评论
    @Select("SELECT * FROM replys WHERE cid=#{commentId} ORDER BY create_time ASC")
    @ResultMap("reply")
    List<Reply> selectByCommentId(@Param("commentId") Integer commentId);

    //查找所有子评论
    @Select("SELECT * FROM replys")
    @Results(id = "reply",value = {
            @Result(property = "id",column = "id",jdbcType = JdbcType.INTEGER),
            @Result(property = "commentId",column = "cid",jdbcType = JdbcType.INTEGER),
            @Result(property = "toId",column = "to_id",jdbcType = JdbcType.INTEGER),
            @Result(property = "userName",column = "cname",jdbcType = JdbcType.VARCHAR),
            @Result(property = "userEmail",column = "email",jdbcType = JdbcType.VARCHAR),
            @Result(property = "content",column = "content",jdbcType = JdbcType.VARCHAR),
            @Result(property = "likerNum",column = "liker_num",jdbcType = JdbcType.VARCHAR),
            @Result(property = "createTime",column = "create_time",jdbcType = JdbcType.TIMESTAMP)})
    List<Reply> selectAll();
}
