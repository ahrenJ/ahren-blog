package cn.ahren.blog.dao;

import cn.ahren.blog.entity.Comment;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface CommentDao {

    //添加评论
    @Insert("INSERT INTO comments(id,aid,cname,email,content) " +
            "VALUES(#{id},#{articleId},#{userName},#{userEmail},#{content})")
    int insert(Comment comment);

    //删除评论
    @Delete("DELETE FROM comments WHERE id=#{id}")
    int delete(@Param("id") Integer id);

    //增加1条回复数
    @Update("UPDATE comments SET reply_num=reply_num+1 WHERE id=#{id}")
    int incReplyNum(@Param("id")Integer id);

    //减少1条回复数
    @Update("UPDATE comments SET reply_num=reply_num-1 WHERE id=#{id}")
    int decReplyNum(@Param("id")Integer id);

    //查找评论
    @Select("SELECT * FROM comments WHERE id=#{id}")
    @ResultMap("comment")
    Comment selectById(@Param("id") Integer id);

    //根据文章id查找评论
    @ResultMap("comment")
    @Select("SELECT * FROM comments WHERE aid=#{articleId}")
    List<Comment> selectByArticleId(@Param("articleId") String articleId);

    //查找所有评论
    @Select("SELECT * FROM comments")
    @Results(id = "comment",value = {
            @Result(property = "id",column = "id",jdbcType = JdbcType.INTEGER),
            @Result(property = "articleId",column = "aid",jdbcType = JdbcType.VARCHAR),
            @Result(property = "userName",column = "cname",jdbcType = JdbcType.VARCHAR),
            @Result(property = "userEmail",column = "email",jdbcType = JdbcType.VARCHAR),
            @Result(property = "content",column = "content",jdbcType = JdbcType.VARCHAR),
            @Result(property = "likerNum",column = "liker_num",jdbcType = JdbcType.INTEGER),
            @Result(property = "replyNum",column = "reply_num",jdbcType = JdbcType.INTEGER),
            @Result(property = "createTime",column = "create_time",jdbcType = JdbcType.TIMESTAMP)})
    List<Comment> selectAll();
}
