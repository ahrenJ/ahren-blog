package cn.ahren.blog.dao;

import cn.ahren.blog.entity.Article;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ArticleDao {

    //添加文章
    @Insert("INSERT INTO article(id,title,summary,content,cid,state) " +
            "VALUES (#{id},#{title},#{summary},#{content},#{categoryId},#{state})")
    int insert(Article article);

    //根据id查找文章
    @Select("SELECT * FROM article WHERE id=#{id}")
    @ResultMap("article")
    Article selectById(@Param("id") String id);

    //修改文章
    @Update("UPDATE article SET title=#{title},summary=#{summary}," +
            "content=#{content},cid=#{categoryId},state=#{state} "+
            "WHERE id=#{id}")
    int update(Article article);

    //修改文章评论数
    @Update("UPDATE article SET comment_num=#{newNum} WHERE id=#{id}")
    int updateCommentNum(@Param("id")String id,@Param("newNum")Integer newNum);

    //删除文章
    @Delete("DELETE FROM article WHERE id=#{id}")
    int delete(@Param("id")String id);

    //根据类别id查找文章
    @ResultMap("article")
    @Select("SELECT * FROM article INNER JOIN category ON article.cid=category.id " +
            "WHERE cid=#{categoryId}")
    List<Article> selectByCategoryId(@Param("categoryId") Integer categoryId);

    //查找文章（根据文章id集合）
    @ResultMap("article")
    @Select("<script>" +
                "SELECT * FROM article WHERE id IN " +
                "<foreach item='articleId' collection='idList' open='(' separator=',' close=')'>" +
                    "#{articleId}" +
                "</foreach>" +
            "</script>")
    List<Article> selectByIdList(@Param("idList") List<String> idList);

    //查询所有文章
    @Select("SELECT * FROM article")
    @Results(id = "article",value = {@Result(property = "id",column = "id",jdbcType = JdbcType.VARCHAR),
            @Result(property = "title",column = "title",jdbcType = JdbcType.VARCHAR),
            @Result(property = "summary",column = "summary",jdbcType = JdbcType.VARCHAR),
            @Result(property = "content",column = "content",jdbcType = JdbcType.VARCHAR),
            @Result(property = "categoryId",column = "cid",jdbcType = JdbcType.INTEGER),
            @Result(property = "state",column = "state",jdbcType = JdbcType.INTEGER),
            @Result(property = "commentNum",column = "comment_num",jdbcType = JdbcType.INTEGER),
            @Result(property = "likerNum",column = "liker_num",jdbcType = JdbcType.INTEGER),
            @Result(property = "readNum",column = "read_num",jdbcType = JdbcType.INTEGER),
            @Result(property = "createTime",column = "create_time",jdbcType = JdbcType.TIMESTAMP),
            @Result(property = "updateTime",column = "update_time",jdbcType = JdbcType.TIMESTAMP)})
    List<Article> selectAll();

    //文章计数（正稿）
    @Select("SELECT COUNT(*) FROM article WHERE state=0")
    int count();

    //文章计数（正稿）-根据类别id
    @Select("SELECT COUNT(*) FROM article WHERE cid=#{categoryId} AND state=0")
    int countByCategoryId(@Param("categoryId") String categoryId);

    //文章计数（正稿）-根据标题
    @Select("SELECT COUNT(*) FROM article WHERE title LIKE CONCAT('%',#{title},'%') AND state=0")
    int countByTitle(@Param("title") String title);

    //增加阅读数
    @Update("UPDATE article SET read_num=read_num+1 WHERE id=#{id}")
    int incReadNum(@Param("id")String id);

    //增加点赞数
    @Update("UPDATE article SET liker_num=liker_num+1 WHERE id=#{id}")
    int incLikerNum(@Param("id")String id);

    //减少点赞数
    @Update("UPDATE article SET liker_num=liker_num-1 WHERE id=#{id}")
    int decLikerNum(@Param("id")String id);
}
