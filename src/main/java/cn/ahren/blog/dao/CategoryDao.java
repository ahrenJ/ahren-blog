package cn.ahren.blog.dao;

import cn.ahren.blog.entity.Category;
import cn.ahren.blog.vo.CateToArticleNumVo;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CategoryDao {

    //添加分类
    @Insert("INSERT INTO category(cname) VALUES(#{name})")
    int insert(Category category);

    //修改分类
    @Update("UPDATE category SET cname=#{name} WHERE id=#{id}")
    int update(Category category);

    //删除分类
    @Delete("DELETE FROM category WHERE id=#{id}")
    int delete(@Param("id") Integer id);

    //查询分类（根据id）
    @Select("SELECT * FROM category WHERE id=#{id}")
    @ResultMap("category")
    Category selectById(Integer id);

    //查询分类（根据名称）
    @Select("SELECT * FROM category WHERE cname=#{name}")
    @ResultMap("category")
    Category selectByName(@Param("name") String name);

    //查询所有分类及对应的文章数量
    @Select("SELECT category.id,category.cname,COUNT(article.cid) as articleNum " +
            "FROM category LEFT JOIN article ON category.id=article.cid AND article.state=0 " +
            "GROUP BY category.id")
    @Results(value = {
            @Result(property = "id",column = "id",jdbcType = JdbcType.INTEGER),
            @Result(property = "name",column = "cname",jdbcType = JdbcType.VARCHAR),
            @Result(property = "articleNum",column = "articleNum",jdbcType = JdbcType.INTEGER)
    })
    List<CateToArticleNumVo> countByArticleNum();

    //查询所有分类
    @Select("SELECT * FROM category")
    @Results(id = "category",value = {
            @Result(property = "id",column = "id",jdbcType = JdbcType.INTEGER),
            @Result(property = "name",column = "cname",jdbcType = JdbcType.VARCHAR)})
    List<Category> selectAll();
}
