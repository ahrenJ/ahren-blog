package cn.ahren.blog.aspect;

import cn.ahren.blog.config.BlogConfiguration;
import cn.ahren.blog.exception.AdminAuthorizeException;
import cn.ahren.blog.exception.AdminDuplicateLoginException;
import cn.ahren.blog.exception.IllegalAccessBlogException;
import cn.ahren.blog.utils.CookieUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

//基于AOP的权限验证拦截器
@Component
@Aspect
@Slf4j
public class BlogAOPInterceptor {

    @Autowired
    private StringRedisTemplate redisTemplate;

    //定义切点（类的所有或部分方法）
    //拦截所有后台系统接口，检验用户是否已经登录管理员账户
    @Pointcut("execution(public * cn.ahren.blog.controller.admin.*.*(..))"+
    "&&!execution(public * cn.ahren.blog.controller.admin.LoginController.*(..))")
    public void authoVerify(){}

    //定义切点
    //拦截用户进入登录界面，检验用户当前是否已登录管理员账户，如已登录直接跳转不再进入登录界面
    @Pointcut("execution(public * cn.ahren.blog.controller.admin.BaseController.login(..))")
    public void loginVerify(){}

    //定义切点
    //检验博客前台是否开放
    @Pointcut("execution(public * cn.ahren.blog.controller.blog.*.*(..))")
    public void validIsOpen(){}

    //定义前置增强（进入切点的方法之前调用的方法）
    //后台系统页面拦截
    @Before("authoVerify()")
    public void doAuthoVerify(){
        ServletRequestAttributes attributes=(ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request=attributes.getRequest();
        //查询Cookie
        Cookie cookie=CookieUtil.get(request,"admin_token");
        if (cookie==null){
            log.warn("[Admin-后台权限拦截]：未登录，无法进入后台系统");
            throw new AdminAuthorizeException();
        }
    }

    //登录界面拦截
    @Before("loginVerify()")
    public void doLoginVerify(){
        ServletRequestAttributes attributes=(ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request=attributes.getRequest();
        //查询Cookie
        Cookie cookie=CookieUtil.get(request,"admin_token");
        if (cookie!=null){
            log.info("[Admin-登录界面拦截]：管理员已登录，即将进入系统");
            throw new AdminDuplicateLoginException();
        }
    }

    //检验博客前台是否开放，如开放则设置Cookie
    @Before("validIsOpen()")
    public void doValidIsOpen(){

        ServletRequestAttributes attributes=(ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request=attributes.getRequest();
        HttpServletResponse response=attributes.getResponse();
        //检验博客前台是否开放（持有管理员登录过后台的Cookie可特殊进入）
        String isOpen= (String) redisTemplate.opsForHash().get(BlogConfiguration.KEY,BlogConfiguration.HASH_KEY_OPEN_OUTSIDE);
        if (!StringUtils.isEmpty(isOpen) && Integer.parseInt(isOpen)==0) {
            if (CookieUtil.get(request,"admin_token")!=null){
                log.warn("允许管理员进入已关闭的博客前台");
            }else {
                throw new IllegalAccessBlogException();
            }
        }else if (BlogConfiguration.OPEN_OUTSIDE==0){
            throw new IllegalAccessBlogException();
        }

        Cookie cookie=CookieUtil.get(request,"blog_token");
        if (cookie==null){
            log.info("[Admin-博客前台]：新用户");
            String token=UUID.randomUUID().toString();
            CookieUtil.set("blog_token",response,token,24*15);
        }
    }
}
