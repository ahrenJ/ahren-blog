package cn.ahren.blog.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

//Cookie设置工具类
public class CookieUtil {

    public static Integer DEFAULT_EXPIRE=3600;   //单位：小时

    //设置Cookie
    public static void set(String name,HttpServletResponse response, String value,int aliveHours){
        Cookie cookie=new Cookie(name,value);
        cookie.setPath("/");
        cookie.setMaxAge(DEFAULT_EXPIRE * aliveHours);
        response.addCookie(cookie);
    }

    //获取Cookie
    public static Cookie get(HttpServletRequest request,String name){
        Map<String,Cookie> cookieMap=readCookie(request);
        if (cookieMap.containsKey(name)){
            return cookieMap.get(name);
        }
        return null;
    }

    //遍历Cookies并返回Map
    private static Map<String,Cookie> readCookie(HttpServletRequest request){
        Cookie[] cookies=request.getCookies();
        Map<String,Cookie> cookieMap=new HashMap<>();
        if (cookies!=null){
            for (Cookie cookie : cookies){
                cookieMap.put(cookie.getName(),cookie);
            }
        }
        return cookieMap;
    }
}
