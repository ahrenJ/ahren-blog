package cn.ahren.blog.utils;

import java.util.Random;

//文章id生成工具
public class KeyGenerator {

    public static String genKey(){
        StringBuilder builder=new StringBuilder();
        //ID组成一：服务器当前时间戳
        builder.append(String.valueOf(System.currentTimeMillis()));
        //ID组成二：0-999随机数
        int randNum=new Random().nextInt(900)+100;
        builder.append(String.valueOf(randNum));
        return builder.toString();
    }
}
