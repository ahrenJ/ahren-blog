package cn.ahren.blog.utils;

import cn.ahren.blog.dao.ArticleTagsDao;
import cn.ahren.blog.dao.CategoryDao;
import cn.ahren.blog.dao.TagDao;
import cn.ahren.blog.dto.ArticleDto;
import cn.ahren.blog.entity.Article;
import cn.ahren.blog.entity.ArticleTags;
import cn.ahren.blog.entity.Category;
import cn.ahren.blog.entity.Tag;
import cn.ahren.blog.enums.BlogErrorEnum;
import cn.ahren.blog.exception.BlogException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.stream.Collectors;

//ArticleDto与Article（或集合）转换工具
@Slf4j
@Component
public class ArticleConvertor {

    private static CategoryDao categoryDao;
    private static TagDao tagDao;
    private static ArticleTagsDao articleTagsDao;
    //静态变量注入需要使用setter方法
    @Autowired
    private void setCategoryDao(CategoryDao categoryDao){
       ArticleConvertor.categoryDao=categoryDao;
    }
    @Autowired
    private void setTagDao(TagDao tagDao){
        ArticleConvertor.tagDao=tagDao;
    }
    @Autowired
    private void setArticleTagsDao(ArticleTagsDao articleTagsDao){
        ArticleConvertor.articleTagsDao=articleTagsDao;
    }

    //转换：ArticleDto => Article
    public static Article convertToArticle(ArticleDto articleDto)throws Exception{
        //获取类别对象
        Category category=categoryDao.selectByName(articleDto.getCategoryName());
        if (ObjectUtils.isEmpty(category)){
            log.error("[Admin-文章服务]-错误：{}",BlogErrorEnum.CATEGORY_NO_EXIST.getMsg());
            throw new BlogException(BlogErrorEnum.CATEGORY_NO_EXIST);
        }
        Article article=new Article();
        BeanUtils.copyProperties(articleDto,article);
        article.setCategoryId(category.getId());
        return article;
    }

    //转换： Article => ArticleDto
    public static ArticleDto convertToArticleDto(Article article)throws Exception{
        //获取类别对象
        Category category=categoryDao.selectById(article.getCategoryId());
        if (ObjectUtils.isEmpty(category)){
            log.error("[Admin-文章服务]-错误：{}",BlogErrorEnum.CATEGORY_NO_EXIST.getMsg());
            throw new BlogException(BlogErrorEnum.CATEGORY_NO_EXIST);
        }
        ArticleDto articleDto=new ArticleDto();
        //获取标签id集合
        List<ArticleTags> articleTags=articleTagsDao.selectByArticleId(article.getId());
        if (!CollectionUtils.isEmpty(articleTags)){
            List<Integer> tagIds=articleTags.stream().map(at->at.getTagId()).collect(Collectors.toList());
            List<Tag> tags=tagDao.selectByIdList(tagIds);
            articleDto.setTags(tags);
        }
        //组成ArticleDto
        BeanUtils.copyProperties(article,articleDto);
        articleDto.setCategoryName(category.getName());
        return articleDto;
    }

    //查询每篇文章对应的List<Tag>标签集合并注入List<ArticleDto>集合中的每个元素
    public static void setTagsToArticleDto(List<ArticleDto> articleDtos){
        //获取每篇文章对应的标签集合
        for(ArticleDto articleDto : articleDtos){
            List<ArticleTags> articleTags=articleTagsDao.selectByArticleId(articleDto.getId());
            if (CollectionUtils.isEmpty(articleTags))
                continue;
            List<Integer> tagIds=articleTags.stream().map(at->at.getTagId()).collect(Collectors.toList());
            List<Tag> tags=tagDao.selectByIdList(tagIds);
            articleDto.setTags(tags);
        }
    }
}
