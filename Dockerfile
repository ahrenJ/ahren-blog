FROM java
MAINTAINER ahrenJ
COPY blog-0.0.1-SNAPSHOT.jar /webApp/blog-0.0.1-SNAPSHOT.jar
CMD java -jar /webApp/blog-0.0.1-SNAPSHOT.jar
EXPOSE 8080
